<?php

namespace App\Console\Commands;

use App\Models\Product;
use Dawson\Youtube\Facades\Youtube;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Vimeo\Laravel\VimeoManager;

class PushVideoAuto extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'upload:video';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upload video to youtube';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(VimeoManager $vimeo)
    {
        $product = Product::select('id', 'name', 'video_url', 'description')
            ->where('status', config('common.product_status.success'))
            ->where('video_url', 'LIKE', '%' . 'drive.google.com' . '%')
            ->first();
        if (!empty($product)) {
            $product = $product->toArray();
            $newStr = substr($product['video_url'], strrpos($product['video_url'], 'd/'));
            $fileCode = trim(trim($newStr, 'd/'), '/view?usp=sharing');
            $storage = Storage::disk('google');
            $content = $storage->get($fileCode);
            $filePath = '/download/video.mp4';
            Storage::disk('public')->put($filePath, $content);
            $file = public_path() . '/storage' . $filePath;
            $video = Youtube::upload($file, [
                'title'       => $product['name'],
                'description' => 'Miêu tả ' . $product['name'],
                'tags'          => ['foo', 'bar', 'baz'],
            ]);
            $product = Product::find($product['id']);
            $product->update(['video_url' => 'https://www.youtube.com/embed/'.$video->getVideoId()]);
            Storage::disk('public')->delete($filePath);
        }
    }
}
