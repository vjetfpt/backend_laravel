<?php

namespace App\Console\Commands;

use App\Http\Libraries\Mail;
use App\Http\Libraries\RedisQueue;
use App\Models\Product;
use App\Models\Transcript;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Str;

class SendMailStudent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendmail:student';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send mail notify student post assigment';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->mail = new Mail();
        parent::__construct();
    }
    private function createProduct($teacherId, $subjectId, $campusId, $semesterId)
    {
        $token = Str::random(12);
        Product::create([
            'teacher_id' => $teacherId,
            'subject_id' => $subjectId,
            'semester_id' => $semesterId,
            'campus_id' => $campusId,
            'token' => $token
        ]);
        return $token;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $redis = new RedisQueue();
        $data = $redis->lpop('records');
        $count_email = 0;
        $count_end = 5;
        while ($data != null) {
            $data_decode = json_decode($data);
            try {
                DB::beginTransaction();
                if ($data_decode->point >= 9 && $data_decode->point <= 10) {
                    $user = User::where('email', $data_decode->email);
                    if (!$user->exists()) {
                        $code = filter_var($data_decode->email, FILTER_SANITIZE_NUMBER_INT);
                        $indexCode = strpos($data_decode->email, $code);
                        $prefixCode = substr($data_decode->email, $indexCode - 2, 2);
                        $studentCode = $prefixCode . $code;
                        $idUser = User::create([
                            'name' => $data_decode->name,
                            'email' => $data_decode->email,
                            'campus_id' => $data_decode->campus_id,
                            'student_code' => $studentCode,
                            'is_active' => 1,
                            'type' => config('common.user_type.student')
                        ])->id;
                    } else {
                        $getUser = $user->get();
                        $idUser = $getUser[0]->id;
                    }
                    $transcript = Transcript::where('user_id', $idUser)
                        ->where('subject_code', $data_decode->subject_code);
                    if (!$transcript->exists()) {
                        Transcript::create([
                            'user_id' => $idUser,
                            'score' => $data_decode->point,
                            'subject_code' => $data_decode->subject_code,
                            'teacher_name' => $data_decode->teacher,
                            'status' => config('common.send_mail.status.sent'),
                            'campus_id' => $data_decode->campus_id,
                            'semester_id' => $data_decode->semester_id
                        ]);
                    } else {
                        $transcript->update(
                            [
                                'score' => $data_decode->point
                            ]
                        );
                    }
                    $teacherId = DB::table('users')
                        ->where('name', $data_decode->teacher)->first()->id;
                    $subjectId = DB::table('subjects')
                        ->where('code', $data_decode->subject_code)->first()->id;
                    $token = $this->createProduct(
                        $teacherId,
                        $subjectId,
                        $data_decode->campus_id,
                        $data_decode->semester_id
                    );
                    $this->mail->createSendMailStudent('SEND_MAIL_STUDENT', [
                        'email'  =>  $data_decode->email,
                        'name'   => $data_decode->name,
                        'teacher_code' => $teacherId,
                        'token' => $token,
                        'subject_code' => $data_decode->subject_code,
                        'teacher'  => $data_decode->teacher
                    ]);
                    $count_email++;
                    if ($count_email >= $count_end) {
                        break;
                    }
                }
                DB::commit();
            } catch (Exception $error) {
                DB::rollBack();
                throw $error;
            }
            $data = $redis->lpop('records');
        }
        Log::debug('Run count mail ' . $count_email);
    }
}
