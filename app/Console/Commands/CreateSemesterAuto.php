<?php

namespace App\Console\Commands;

use App\Models\Semester;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CreateSemesterAuto extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:semester';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create semester';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $yearNow = date("Y");
        $monthNow = date("m");
        $nameSemester = '';

        if ($monthNow < 4) {
            $nameSemester = "Spring " . $yearNow;
        } elseif ($monthNow < 9) {
            $nameSemester = "Summer " . $yearNow;
        } else {
            $nameSemester = "Fall " . $yearNow;
        }
        $nameSemesterDb = DB::table('semesters')->latest('created_at')
            ->first()->name;
        if ($nameSemesterDb != $nameSemester) {
            Semester::create(['name' => $nameSemester]);
        }
    }
}
