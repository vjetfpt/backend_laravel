<?php
namespace App\Repositories\Semester;
use App\Interfaces\Semester\SemesterInterface;
use App\Models\Semester;
use App\Repositories\BaseRepository\BaseRepository;

class SemesterRepository extends BaseRepository implements SemesterInterface {
    public function getModel()
    {
        return Semester::class;
    }
    public function getDataPangination(){
        return $this->model->simplePaginate(6);
    }
}
