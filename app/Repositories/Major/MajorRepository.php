<?php
namespace App\Repositories\Major;
use App\Interfaces\Major\MajorInterface;
use App\Models\Major;
use App\Models\Subject;
use App\Repositories\BaseRepository\BaseRepository;

class MajorRepository extends BaseRepository  implements MajorInterface {

    public  function getModel()
    {
        return Major::class;
    }

    public function deleteChildren($id)
    {
        $subject = Subject::where('major_id', $id)->delete();
        return $subject;
    }
}
