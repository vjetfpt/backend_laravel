<?php

namespace App\Repositories\User;

use App\Interfaces\User\UserInterface;
use App\Models\Product;
use App\Models\Subject;
use App\Models\User;
use App\Repositories\BaseRepository\BaseRepository;
use App\Models\UserMajor;
use App\Rules\excel\checkEmail;
use App\Rules\excel\checkSubjectCode;
use App\Rules\excel\checkTeacherName;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Str;
use App\Http\Libraries\RedisQueue;

class UserRepository extends BaseRepository implements UserInterface, ToModel, WithHeadingRow
{
    public function getModel()
    {
        return User::class;
    }
    public function model(array $row)
    {
        // Validator::make($row, [
        //     'email' => ['required', new checkEmail($row['email'])],
        //     'ma_mon' => ['required', new checkSubjectCode($row['ma_mon'])],
        //     'giang_vien' => ['required', new checkTeacherName($row['giang_vien'], $row['ma_mon'])],
        //     'ho_ten' => 'required',
        //     'diem' => 'required|numeric|min:9|max:10',
        // ], [
        //     'required' => ':attribute không được để trống',
        //     'diem.numeric' => 'Điểm không phải dạng số',
        //     'diem.min' => 'Điểm nhập vào không đạt',
        //     'diem.max' => 'Điểm nhập vào không đạt',
        // ], [
        //     'email' => 'Email',
        //     'ma_mon' => 'Mã môn',
        //     'giang_vien' => 'Tên giảng viên',
        //     'ho_ten' => 'Họ tên',
        //     'diem' => 'Điểm',
        // ])->validated();
        $redis_queue = new RedisQueue();
        $row['token'] = Str::random(12);
        $redis_queue->lpush('records', json_encode([
            'email' => $row['email'],
            'name' => $row['ho_ten'],
            'point' => $row['diem'],
            'subject_code' => $row['ma_mon'],
            'teacher'  => $row['giang_vien'],
            'token' => $row['token'],
            'campus_id' => request()->input('campus_id'),
            'semester_id' => request()->input('semester_id')
        ]));
    }

    public function getProduct($userId)
    {
        $listProduct = Product::where('teacher_id', $userId);
        $arrProduct = $listProduct->get()->load(['teacher', 'subject', 'students', 'productGalleries', 'major'])
            ->toArray();
        $userMasterCheck = User::where('id', $userId)
            ->where(
                'type',
                config('common.user_type.master_teacher')
            );
        if ($userMasterCheck->exists()) {
            $listProductId = $listProduct->pluck('id')->toArray();
            $listMajorIdManage = UserMajor::where('user_id', $userId)
                ->pluck('major_id')->toArray();
            $listSubjectIdManage = Subject::whereIn('major_id', $listMajorIdManage)
                ->pluck('id')->toArray();
            $arrProductException = Product::whereIn('subject_id', $listSubjectIdManage)
                ->whereNotIn('id', $listProductId)
                ->whereIn('status', 
                        [config('common.product_status.wait_confirm2'),
                        config('common.product_status.success')])->get()
                ->load(['teacher', 'subject', 'students', 'productGalleries','major'])->toArray();
            $arrProduct = array_merge($arrProduct, $arrProductException);
        }
        return $arrProduct;
    }

}
