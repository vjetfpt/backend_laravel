<?php

namespace App\Repositories\CommentRepository;

use App\Interfaces\CommentInterface\CommentInterface;
use App\Models\Comment;
use App\Repositories\BaseRepository\BaseRepository;
use Google\Service\Docs\Request;
use Illuminate\Support\Facades\DB;

class CommentRepository extends BaseRepository implements CommentInterface
{
    public function getModel()
    {
        return Comment::class;
    }

    public function comment($data)
    {
        $comment = $data['comment'];
        $array = ['dcm', 'vl', 'cc', 'cdb'];
        foreach ($array as $item) {
            if (str_contains($comment, $item)) {
                return false;
            }
        }
        $save = Comment::create($data);
        $save->load('getInfoUser');
        return $save;
    }

    public function getComments()
    {
        $list_comment = Comment::with('getInfoUser')->where('parent_id', 0)->get();
        $list_comment->load('getInfoProduct');
        $list_comment->load('getReply.getInfoUser');
        return $list_comment;
    }

    public function reply($data)
    {
        $array = ['dcm', 'vl', 'cc', 'cdb'];
        foreach ($array as $item) {
            if (str_contains($data['comment'], $item)) {
                return false;
            }
        }
        if ($data['comment_id']) {
            $data['parent_id'] = $data['comment_id'];
            unset($data['comment_id']);
        }
        $save = Comment::create($data);
        $save->load('getInfoUser');
        return $save;
    }

    public function editComment($data)
    {

        $check = DB::table('comments')->where('id', $data['comment_id'])->where('user_id', $data['user_id'])->where('product_id', $data['product_id']);
        if (!$check) {
            return false;
        }
        $array = ['dcm', 'vl', 'cc', 'cdb'];
        foreach ($array as $item) {
            if (str_contains($data['comment'], $item)) {
                return false;
            }
        }
        $id = $data['comment_id'];
        unset($data['user_id']);
        unset($data['comment_id']);
        unset($data['product_id']);
        $update = $check->update($data);
        $dataUpdate = Comment::with('getInfoUser')->where('id', $id)->get();

        return $dataUpdate;
    }

    public function editReply($data)
    {
        $array = ['dcm', 'vl', 'cc', 'cdb'];
        foreach ($array as $item) {
            if (str_contains($data['comment'], $item)) {
                return false;
            }
        }
        $check = DB::table('comments')->where('parent_id', $data['parent_id'])->where('user_id', $data['user_id']);
        $id = $data['comment_id'];
        unset($data['product_id']);
        unset($data['parent_id']);
        unset($data['user_id']);
        unset($data['comment_id']);
        $update_reply = $check->update($data);
        $data_reply = Comment::with('getInfoUser')->where('id', $id)->get();
        return $data_reply;
    }

    public function getCommentsOfProduct($data)
    {
        $get_comment_of_product = Comment::with('getInfoUser')->where('parent_id', 0)->where('product_id', $data['product_id'])->get();
        $get_comment_of_product->load('getReply.getInfoUser');
        return $get_comment_of_product;
    }

    public function deleteComment($data)
    {
        $delete = DB::table('comments')->where('id', $data['comment_id'])->where('user_id', $data['user_id']);
        if (!$delete) {
            return false;
        }
        $delete_item = DB::table('comments')->where('parent_id', $data['comment_id'])->delete();
        $delete->delete();
        return true;
    }

    public function deleteReply ($data)
    {
        $delete = DB::table('comments')->where('id', $data['comment_id'])->where('user_id', $data['user_id'])->delete();
        return true;
    }

    public function getNewComments()
    {
        $new_comments = Comment::with('getInfoUser')->where('parent_id', 0)->orderBy('id', 'desc')->limit(10)->get();
        $new_comments->load('getInfoProduct');
        return $new_comments;
    }

}
