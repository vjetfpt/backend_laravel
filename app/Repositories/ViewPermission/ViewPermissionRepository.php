<?php
namespace App\Repositories\ViewPermission;
use App\Repositories\BaseRepository\BaseRepository;
use App\Interfaces\ViewPermission\ViewPermissionInterface;
use App\Models\ViewPermission;

class ViewPermissionRepository extends BaseRepository  implements ViewPermissionInterface {

    public function getModel()
    {
        return ViewPermission::class;
    }
}
