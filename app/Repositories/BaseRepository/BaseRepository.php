<?php

namespace App\Repositories\BaseRepository;

use App\Interfaces\BaseInterface\BaseInterface;

abstract class BaseRepository implements BaseInterface
{

    protected $model;

    public function __construct()
    {
        $this->setModel();
    }


    abstract public function getModel();

    /**
     * Set model
     */
    public function setModel()
    {
        $this->model = app()->make($this->getModel());
    }

    public function getAll()
    {
        return $this->model->all();
    }

    public function find($id)
    {
        $result = $this->model->find($id);

        return $result;
    }

    public function create($attributes = [])
    {
        return $this->model->create($attributes);
    }

    public function update($id, $attributes = [])
    {
        $result = $this->find($id);
        if ($result) {
            $result->update($attributes);
            return $result;
        }
        return false;
    }

    public function delete($id)
    {
        $result = $this->find($id);
        if ($result) {
            $result->delete();
            return true;
        }
        return false;
    }

    public function getDataPaginate($page, $pageLength, $collection = [])
    {
        if(!empty($collection)){
            $collection = $collection;
            $check = 1;
        }
        else{
            $collection = $this->getAll();
            $check = 0;
        }
        if ($page > 0 && $pageLength > 0) {
            $records = $collection;
            $totalRecord = count($records);
            $pages = ceil($totalRecord / $pageLength);
            $offset = ($page * $pageLength) - $pageLength;
            if($page <= $pages && $offset >= 0){
                if($check){
                    $data = $collection->slice($offset, $pageLength);
                }
                else{
                    $data = $this->model->offset($offset)
                                    ->limit($pageLength)->get();
                }
                return $data;
            }
        }
        return [];
    }

    public function countRecord()
    {
        return $this->model->count();
    }
}
