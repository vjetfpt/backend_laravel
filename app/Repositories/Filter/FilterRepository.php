<?php

namespace App\Repositories\Filter;

use App\Interfaces\Filter\FilterInterface;
use App\Models\Major;
use App\Models\Product;
use App\Models\User;
use App\Models\UserMajor;
use App\Repositories\BaseRepository\BaseRepository;

class FilterRepository extends BaseRepository implements FilterInterface
{

    public function getModel()
    {
        return Product::class;
    }

    public function searchProductStudentId($text)
    {
        $userId = User::where('student_code', 'LIKE', "%$text%")
            ->first()->id ?? null;
        $products = Product::where('user_id', $userId)
                            ->where('status',config('common.product_status.success'))
                            ->get();
        if (!empty($products->toArray())) {
            return $products->load(['teacher', 'subject', 'students', 'productGalleries', 'major']);
        } else {
            return null;
        }
    }

    public function searchProductName($text)
    {
        $products = Product::where('name', 'LIKE', "%$text%")
                            ->where('status',config('common.product_status.success'))
                            ->get();
        if (!empty($products->toArray())) {
            return $products->load(['teacher', 'subject', 'students', 'productGalleries', 'major']);
        } else {
            return null;
        }
    }

    public function filterId($id, $type)
    {
        switch ($type) {
            case 'semester':
                $products = Product::where('semester_id', $id)
                                    ->where('status',config('common.product_status.success'))
                                    ->get() ?? null;
                $listProduct = $products != null ? $products->load(['teacher', 'subject', 'students', 'productGalleries', 'major'])
                    : null;
                return $listProduct;
            break;
            case 'major':
                $listProduct = Major::find($id)->products
                                    ->load(['teacher', 'subject', 'students', 'productGalleries', 'major'])->toArray() ?? null;
                return $listProduct;
            break;
            case 'campus':
                $products = Product::where('campus_id', $id)
                                    ->where('status',config('common.product_status.success'))
                                    ->get() ?? null;
                $listProduct = $products != null ? $products->load(['teacher', 'subject', 'students', 'productGalleries', 'major'])
                    : null;
                return $listProduct;
            break;
            case 'master_user':
                $listMajorId = UserMajor::select('major_id')->where('user_id', $id)
                                        ->pluck('major_id')->toArray() ?? null;
                $listMajor = Major::whereIn('id',$listMajorId)
                                    ->get() ?? null;
                $listProduct = $listMajor->map(function($item,$key){
                    return $item->products
                        ->load(['teacher', 'subject', 'students', 'productGalleries', 'major'])->toArray();
                });
                return $listProduct;
            break;
            case 'teacher_user_major':
                $listUserId = UserMajor::where('major_id', $id)
                                        ->pluck('user_id');
                if(!empty($listUserId)){
                    $listTeacher = User::whereIn('id', $listUserId)
                                        ->get() ?? [];
                    return $listTeacher;
                }
                return [];
            break;
            case 'subject':
                $products = Product::where('subject_id', $id)->get() ?? null;
                $listProduct = $products != null ? $products->load(['teacher', 'subject', 'students', 'productGalleries', 'major'])
                    : [];
                return $listProduct;
            break;
            default:
                return null;
            break;
        }
    }

    public function fitlerStatusProduct($status){
        $listProduct = Product::where('status', $status)
                                ->get()->load(['teacher', 'subject', 'students', 'productGalleries', 'major']) ?? null;
        return $listProduct;
    }
}
