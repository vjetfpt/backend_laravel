<?php

namespace App\Repositories\Product;

use App\Interfaces\Product\ProductInterface;
use App\Models\Campus;
use App\Models\Product;
use App\Models\ProductGallery;
use App\Models\Semester;
use App\Models\StudentGroup;
use App\Models\Subject;
use App\Models\Transcript;
use App\Models\User;
use App\Repositories\BaseRepository\BaseRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str as Strs;

class ProductRepository extends BaseRepository implements ProductInterface
{
    private function subGetInfo($product)
    {
        $subjectName = Subject::find($product['subject_id'])->name ?? 'null';
        $teacherName = User::find($product['teacher_id'])->name ?? 'null';
        $semesterName = Semester::find($product['semester_id'])->name ?? 'null';
        $campusName = Campus::find($product['campus_id'])->name ?? 'null';
        $email = '';
        if(!empty($product['user_id'])){
            $email = User::find($product['user_id'])->email ?? '';   
        }
        $data = [
            'teacher_id' => $teacherName,
            'semester_id' => $semesterName,
            'subject_id' => $subjectName,
            'campus_id' => $campusName,
            'email' => $email,
        ];
        return $data;
    }
    public function getModel()
    {
        return Product::class;
    }
    public function addMember($listStudent, $saveId)
    {
        foreach ($listStudent as $student) {
            $code = filter_var($student, FILTER_SANITIZE_NUMBER_INT);
            $indexCode = strpos($student, $code);
            $prefixCode = substr($student, $indexCode - 2, 2);
            $studentCode = $prefixCode . $code;
            $nameStudent = substr($student, 0, strpos($student, $studentCode));
            $user = User::where('email', $student . '@fpt.edu.vn');
            if (!$user->exists()) {
                $idUser = User::create([
                    'student_code' => $studentCode,
                    'email' => $student . '@fpt.edu.vn',
                    'name' => $nameStudent,
                    'is_active' => 1,
                    'campus_id' => 1,
                    'type' => config('common.user_type.student')
                ])->id;
                DB::table('student_groups')->insert([
                    'student_id' => $idUser,
                    'product_id' => $saveId
                ]);
                continue;
            }
            DB::table('student_groups')->insert([
                'student_id' => $user->first()->id,
                'product_id' => $saveId
            ]);
        }
    }
    public function getInfo($code)
    {
        if (array_key_exists('token', $code)) {
            if (is_numeric($code['token'])) {
                $product = Product::where('id', $code['token']);
            }
            else{
                $product = Product::where('token', $code['token']);
            }
            if (!$product->exists()) {
                return ['message' => 'Không tìm thấy thông tin sản phẩm có mã này'];
            } else {
                $product = $product->first()
                    ->load('productGalleries')->toArray();
                if ($product) {
                    if (!empty($product['name'])) {
                        $info = $this->subGetInfo($product);
                        $remove = ['teacher_id', 'semester_id', 'subject_id', 'campus_id'];
                        $newProduct = array_diff_key($product, array_flip($remove));
                        return array_merge($newProduct, $info);
                    } else {
                        return $this->subGetInfo($product);
                    }
                } else {
                    return ['message' => 'Không tồn tại sản phẩm'];
                }
            }
        } else {
            return ['message' => 'Trường dữ liệu gửi lên không hợp lệ'];
        }
    }

    public function deleteImage($imageDb, $folder = 'products')
    {
        $fileName = strstr($imageDb, "uploads/$folder");
        $fileRemove = storage_path() . "/app/public/$fileName";
        if (file_exists($fileRemove)) {
            unlink($fileRemove);
        }
    }

    private function deleteGalleires($idProduct)
    {
        $galleriesDb = ProductGallery::where('product_id', $idProduct)
            ->pluck('image_url')->toArray();
        foreach ($galleriesDb as $url) {
            $fileName = strstr($url, 'uploads/product_galleries');
            $fileRemove = storage_path() . "/app/public/$fileName";
            if (file_exists($fileRemove)) {
                unlink($fileRemove);
            }
        }
        ProductGallery::where('product_id', $idProduct)
            ->delete();
    }

    public function deleteDocument($resource_url)
    {
        $storage = Storage::disk('google');
        $storage->delete($resource_url);
    }
    public function updateProduct($data, $product, $idUser)
    {
        $imageDb = $product->image;
        if ($data['image_url'] != $imageDb) {
            $this->deleteImage($imageDb);
        }
        $galleriesDb = ProductGallery::where('product_id', $product->id)
            ->pluck('image_url')->toArray();
        $arrImageIntersect = array_intersect($galleriesDb, $data['galleries']);
        $listImageDelete = array_diff($galleriesDb, $arrImageIntersect);
        $listImageAdd = array_diff($data['galleries'], $arrImageIntersect);
        if (!empty($listImageDelete)) {
            foreach ($listImageDelete as $url) {
                $fileName = strstr($url, 'uploads/product_galleries');
                $fileRemove = storage_path() . "/app/public/$fileName";
                if (file_exists($fileRemove)) {
                    unlink($fileRemove);
                }
                ProductGallery::where('image_url', $url)->delete();
            }
        }
        if (!empty($listImageAdd)) {
            foreach ($listImageAdd as $url) {
                ProductGallery::insert(['product_id' => $product->id, 'image_url' => $url]);
            }
        }
        if ($product->resource_url != $data['resource_url']) {
            $this->deleteDocument($product->resource_url);
        }
        $studentGroup = StudentGroup::where('product_id', $product->id);
        if ($studentGroup->exists()) {
            $studentGroup->delete();
        }
        $this->addMember($data['students'], $product->id);
        $data['image'] = $data['image_url'];
        $checkUser = User::find($idUser)->type;
        if ($checkUser == config('common.user_type.student')) {
            $data['token'] = Strs::random(12);
        }
        unset($data['image_url']);
        unset($data['students']);
        unset($data['galleries']);
        $product->update($data);
        return $data;
    }

    public function getProduct($token)
    {
        $product = Product::where('token', $token)->first();
        return $product;
    }

    public function deleteProduct($product)
    {
        $this->deleteImage($product->image);
        $this->deleteGalleires($product->id);
        $this->deleteDocument($product->resource_url);
        $this->delete($product->id);
        return true;
    }

    public function rating($data)
    {
        $product = Product::where('id', $data['product_id'])->first();
        $check = DB::table('ratings')->where('rateable_id', $data['product_id'])
            ->where('user_id', $data['user_id'])
            ->first();
        if ($check) {
            $star = $product->rateOnce($data['star']);
        } else {
            $star = $product->rate($data['star']);
        }
        return $star;
    }

    public function avgStar($data)
    {
        $product = Product::where('id', $data['product_id'])->first();
        $avgStar = $product->averageRating();
        return $avgStar;
    }

    public function countStarInProduct($data)
    {
        $countStar = [];
        $query = DB::table('ratings')->where('rateable_id', $data['product_id']);

        if ($query) {
            $countStar['oneStar'] = DB::table('ratings')->where('rateable_id', $data['product_id'])->where('rating', 1)->count();
        }
        if ($query) {
            $countStar['twoStar'] = DB::table('ratings')->where('rateable_id', $data['product_id'])->where('rating', 2)->count();
        }
        if ($query) {
            $countStar['threeStar'] = DB::table('ratings')->where('rateable_id', $data['product_id'])->where('rating', 3)->count();
        }
        if ($query) {
            $countStar['fourStar'] = DB::table('ratings')->where('rateable_id', $data['product_id'])->where('rating', 4)->count();
        }
        if ($query) {
            $countStar['fiveStar'] = DB::table('ratings')->where('rateable_id', $data['product_id'])->where('rating', 5)->count();
        }
        return $countStar;
    }
}
