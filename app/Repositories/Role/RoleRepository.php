<?php
namespace App\Repositories\Role;
use App\Interfaces\Role\RoleInterface;
use App\Models\Role;
use App\Repositories\BaseRepository\BaseRepository;

class RoleRepository extends BaseRepository  implements RoleInterface {

    public  function getModel()
    {
        return Role::class;
    }
}
