<?php
namespace App\Repositories\Campus;
use App\Interfaces\Campus\CampusInterface;
use App\Models\Campus;
use App\Repositories\BaseRepository\BaseRepository;

class CampusRepository extends BaseRepository implements CampusInterface {
    public function getModel()
    {
        return Campus::class;
    }
}
