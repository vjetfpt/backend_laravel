<?php

namespace App\Repositories\Subject;

use App\Interfaces\Subject\SubjectInterface;
use App\Models\Major;
use App\Models\Subject;
use App\Models\User;
use App\Models\UserMajor;
use App\Repositories\BaseRepository\BaseRepository;

class SubjectRepository extends BaseRepository implements SubjectInterface
{

    public  function getModel()
    {
        return Subject::class;
    }

    public function findChairmanMajor($majorId)
    {
        $listUserId = UserMajor::select('user_id')->where('major_id', $majorId)
            ->pluck('user_id')->toArray();
        $user = User::whereIn('id', $listUserId)
            ->where('type', config('common.user_type.master_teacher'))->first();
        return $user;
    }
}
