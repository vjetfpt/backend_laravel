<?php
namespace App\Repositories\FeedBack;
use App\Interfaces\FeedBack\FeedBackInterface;
use App\Models\Feedbacks;
use App\Repositories\BaseRepository\BaseRepository;

class FeedBackRepository extends BaseRepository implements FeedBackInterface {
    public function getModel()
    {
        return Feedbacks::class;
    }
}
