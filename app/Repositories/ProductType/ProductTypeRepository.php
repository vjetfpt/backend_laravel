<?php

namespace App\Repositories\ProductType;

use App\Interfaces\ProductType\ProductTypeInterface;

use App\Models\ProductType;
use App\Repositories\BaseRepository\BaseRepository;

class ProductTypeRepository extends BaseRepository implements ProductTypeInterface
{
    public function getModel()
    {
        return ProductType::class;
    }
}
