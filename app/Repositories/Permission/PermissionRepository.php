<?php
namespace App\Repositories\Permission;
use App\Repositories\BaseRepository\BaseRepository;
use App\Interfaces\Permission\PermissionInterface;
use App\Models\Permission;

class PermissionRepository extends BaseRepository  implements PermissionInterface {

    public  function getModel()
    {
        return Permission::class;
    }
    
}
