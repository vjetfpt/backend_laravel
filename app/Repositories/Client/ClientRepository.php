<?php

namespace App\Repositories\Client;

use App\Interfaces\Client\ClientInterface;
use App\Models\Major;
use App\Models\Product;
use App\Repositories\BaseRepository\BaseRepository;
use Illuminate\Http\Request;
use willvincent\Rateable\Rating;

class ClientRepository extends BaseRepository implements ClientInterface
{
    public function getModel()
    {
        return Product::class;
    }
    public function getProduct()
    {
        $productId = Rating::orderBy('rating', 'Desc')->limit(14)
            ->get()->pluck('rateable_id')->toArray();
        $listProductMore = [];
        if (empty($productId)) {
            $listProduct = Product::limit(14)
                ->where('status', config('common.product_status.success'))
                ->get()->load(['rates'])->toArray();
        } else {
            $listProduct = Product::whereIn('id', $productId)
                ->where('status', config('common.product_status.success'))
                ->get()->load(['rates'])->toArray();
            $countProduct = count($listProduct);
            if ($countProduct < 14) {
                $listProductMore = Product::whereNotIn('id', $productId)
                    ->where('status', config('common.product_status.success'))
                    ->limit(14 - $countProduct)->get()->toArray();
            }
        }
        return response([
            'data' => array_merge($listProduct, $listProductMore)
        ]);
    }

    public function getProductMajor($id, $page = null, $pageLength = null)
    {
        $major = Major::find($id);
        $listProduct = $major != null ? $major->productsRecognize
            ->load(['teacher', 'subject', 'students', 'productGalleries']) : [];
        if (!empty($listProduct)) {
            if ($page != null && $pageLength != null) {
                return $this->getDataPaginate($page, $pageLength, $listProduct);
            } else {
                return $listProduct;
            }
        }
        return [];
    }

    public function filter($idCustom, $majorId, $type)
    {
        $listProduct = [];
        $products = $this->getProductMajor($majorId) ?? [];
        if ($type == 'user') {
            $listProduct = !empty($products) ? $products->where('teacher_id', $idCustom) : [];
        } else {
            $listProduct = !empty($products) ? $products->where('campus_id', $idCustom) : [];
        }
        return $listProduct;
    }

    public function sort($sort, $majorId)
    {
        $products = $this->getProductMajor($majorId) ?? [];
        switch ($sort) {
            case 'newest':
                $listProduct = $products->sortByDesc('updated_at');
                break;
            case 'oldest':
                $listProduct = $products->sortBy('updated_at');
                break;
            case 'highest':
                $list = $products->map(function ($item, $key) {
                    $totalRate = 0;
                    if (!$item->rates->isEmpty()) {
                        foreach ($item->rates as $value) {
                            $totalRate += $value['rating'];
                        }
                        $item['totalRate'] = $totalRate;
                    } else {
                        $item['totalRate'] = $totalRate;
                    }
                    return $item;
                });
                $list = $list->sortByDesc('totalRate');
                foreach ($list as $item) {
                    $listProduct[] = $item;
                }
                break;
            default:
                $listProduct = [];
                break;
        }
        $listProductArr = [];
        if (!empty($listProduct)) {
            foreach ($listProduct as $value) {
                $listProductArr[] = $value;
            }
        }
        return $listProductArr;
    }
}
