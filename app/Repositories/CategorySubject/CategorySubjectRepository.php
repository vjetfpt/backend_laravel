<?php
namespace App\Repositories\CategorySubject;
use App\Interfaces\CategorySubject\CategorySubjectInterface;
use App\Models\Catesubject;
use App\Repositories\BaseRepository\BaseRepository;

class CategorySubjectRepository extends BaseRepository implements CategorySubjectInterface {

    public  function getModel()
    {
        return Catesubject::class;
    }
}
