<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
// use Laravel\Passport\HasApiTokens;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */

    protected $guarded = [];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $guard_name = 'sanctum';

    protected $appends = ['campus'];

    public function transcripts(){
        return $this->hasMany(Transcript::class,'user_id','id');
    }

    public function role(): BelongsToMany
    {
        return $this->belongsToMany(Role::class,'model_has_roles', "model_id");
    }

    public function campuses(){
        return $this->belongsTo(Campus::class, 'campus_id');
    }

    public function getCampusAttribute()
    {
        return $this->getCampus();
    }

    public function getCampus()
    {
        $user = $this;
        if(isset($user->campuses->code)){
            $code_campus = $user->campuses->code;
            if($code_campus){
                $user_campus['campus'] = "";
                switch($code_campus){
                    case "FPT-HN":
                        return $user_campus['campus'] = $code_campus;
                    case "FPT-DN":
                        return $user_campus['campus'] = $code_campus;
                    case "FPT-CT":
                        return $user_campus['campus'] = $code_campus;
                    case "FPT-HCM":
                        return $user_campus['campus'] = $code_campus;
                    case "FPT-TN":
                        return $user_campus['campus'] = $code_campus;
                    default:
                        return [];
                }
            }
        }
        return false;

    }

    public function majors(){
        return $this->belongsToMany(Major::class,'user_majors');
    }
    
    public function getProducts ()
    {
        return $this->belongsToMany(Product::class, 'student_groups', 'student_id', 'product_id');
    }
}
