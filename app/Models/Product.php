<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use willvincent\Rateable\Rateable;
use willvincent\Rateable\Rating;

class Product extends Model
{
    use Rateable;
    use HasFactory;

    use \Znck\Eloquent\Traits\BelongsToThrough;
    protected $table = 'products';
    protected $primaryKey = 'id';
    protected $guarded = [];
    protected $appends = ['master_user', 'comments', 'avg_rate'];
    protected $casts = [
        'created_at' => 'date:Y-m-d',
        'updated_at' => 'date:Y-m-d'
    ];

    public function teacher()
    {
        return $this->belongsTo(User::class, 'teacher_id');
    }

    public function productType()
    {
        return $this->belongsTo(ProductType::class, 'product_type_id');
    }

    public function productGalleries()
    {
        return $this->hasMany(ProductGallery::class, 'product_id');
    }

    public function semester()
    {
        return $this->belongsTo(Semester::class, 'semester_id');
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class, 'subject_id');
    }

    public function students()
    {
        return $this->belongsToMany(User::class, 'student_groups', 'product_id', 'student_id');
    }

    public function cateSubject()
    {
        return $this->belongsToThrough(cateSubject::class, Subject::class);
    }

    public function rates()
    {
        return $this->hasMany(Rating::class, 'rateable_id', 'id');
    }

    public function major()
    {
        return $this->belongsToThrough(Major::class, Subject::class);
    }

    public function masterUser()
    {
        $major = UserMajor::where('user_id', auth()->id())->first();
        if ($major == null) {
            return null;
        }
        $userMasterMajorid = User::where('type', config('common.user_type.master_teacher'))
            ->pluck('id')->toArray();
        $userMajor = UserMajor::whereIn('user_id', $userMasterMajorid)
            ->where('major_id', $major->major_id)->first();
        if (!empty($userMajor)) {
            return $userMajor->user_id;
        } else {
            return null;
        }
    }

    public function getMasterUserAttribute()
    {
        return $this->masterUser();
    }

    public function comments()
    {
        return $this->hasMany(Comment::class, 'product_id');
    }

    public function getCommentsAttribute()
    {
        return $this->comments()->count();
    }

    public function getAvgRateAttribute()
    {
        $product = $this;
        $avgStar = $product->averageRating();
        return $avgStar;
    }
}
