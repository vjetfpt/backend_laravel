<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserMajor extends Model
{
    use HasFactory;

    protected $table = 'user_majors';

}
