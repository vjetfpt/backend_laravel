<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Semester extends Model
{
    use HasFactory;

    protected $table = 'semesters';
    protected $primaryKey = 'id';
    protected $fillable = ['name'];
    public function products(){
        return $this->hasMany(Product::class,'semester_id','id');
    }
}
