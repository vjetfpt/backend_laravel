<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Permission;

class ViewPermission extends Model
{
    use HasFactory;

    protected $table = 'view_permissions';
    protected $primaryKey = 'id';

    protected $guarded = [];
    protected $casts = [
        'created_at' => 'date:Y-m-d',
        'updated_at' => 'date:Y-m-d'
    ];

    public function permission(){
        return $this->belongsTo(Permission::class, 'permission_id');
    }
}
