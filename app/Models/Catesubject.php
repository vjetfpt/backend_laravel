<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Catesubject extends Model
{
    use HasFactory;
    protected $table = 'catesubjects';
    protected $primaryKey = 'id';
    protected $guarded = [];

    protected $casts = [
        'created_at' => 'date:Y-m-d',
        'updated_at' => 'date:Y-m-d'
    ];

    public function subjects()
    {
        return $this->hasMany(Subject::class, 'catesubject_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
