<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Spatie\Permission\Models\Role as Model;
use Spatie\Permission\PermissionRegistrar;

class Role extends Model
{
    use HasFactory;

    protected $table = 'roles';

    public function permission(): BelongsToMany
    {   
        return $this->belongsToMany(Permission::class,'role_has_permissions', "role_id");
    }
}
