<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transcript extends Model
{
    use HasFactory;
    protected $table = 'transcripts';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'score',
        'subject_code',
        'semester_name',
        'teacher_name',
        'status',
        'campus_id',
        'semester_id'
    ];
}
