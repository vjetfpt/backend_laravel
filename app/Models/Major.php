<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Major extends Model
{
    use HasFactory;
    use \Znck\Eloquent\Traits\BelongsToThrough;

    protected $table = 'majors';
    protected $primaryKey = 'id';

    protected $fillable = ['name'];

    protected $casts = [
        'created_at' => 'date:Y-m-d',
        'updated_at' => 'date:Y-m-d'
    ];

    public function subjects()
    {
        return $this->hasMany(Subject::class, 'major_id');
    }

    public function userMajors(){
        return $this->hasMany(UserMajor::class,'major_id');
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = ucwords($value);
    }

    public function getNameAttribute($value)
    {
        return ucwords($value);
    }

    public function products()
    {
        return $this->hasManyThrough(Product::class , Subject::class);
    }

    public function productsRecognize()
    {
        return $this->hasManyThrough(Product::class , Subject::class)
                    ->where('status',config('common.product_status.success'));
    }
}
