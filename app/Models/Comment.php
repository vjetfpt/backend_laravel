<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;
    protected $table = "comments";
    protected $fillable = ['user_id', 'product_id', 'comment', 'parent_id'];

    public function getInfoUser ()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getReply ()
    {
        return $this->hasMany(Comment::class, 'parent_id');
    }

    public function getInfoProduct ()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
