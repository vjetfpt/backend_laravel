<?php
namespace App\Interfaces\Client ;
use App\Interfaces\BaseInterface\BaseInterface;

interface ClientInterface extends BaseInterface {
    public function getProduct();
    public function getProductMajor($id, $page = null, $pageLength = null);
    public function filter($idCustom, $majorId, $type);
    public function sort($sort, $majorId);
}
