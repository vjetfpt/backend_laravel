<?php
namespace App\Interfaces\Major;
use App\Interfaces\BaseInterface\BaseInterface;

interface MajorInterface extends BaseInterface {

    public function deleteChildren($id);

}
