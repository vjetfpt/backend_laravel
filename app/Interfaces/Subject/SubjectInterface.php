<?php
namespace App\Interfaces\Subject;
use App\Interfaces\BaseInterface\BaseInterface;

interface SubjectInterface extends BaseInterface {
    public function findChairmanMajor($majorId);
}
