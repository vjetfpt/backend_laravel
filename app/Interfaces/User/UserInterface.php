<?php
namespace App\Interfaces\User;
use App\Interfaces\BaseInterface\BaseInterface;

interface UserInterface extends BaseInterface {
    public function model(array $row);
    // public function getData();
    public function getProduct($userId);
}
