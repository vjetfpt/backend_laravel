<?php
namespace App\Interfaces\Filter;
use App\Interfaces\BaseInterface\BaseInterface;

interface FilterInterface extends BaseInterface {
    public function searchProductStudentId($text);
    public function searchProductName($text);
    public function filterId($id, $type);
    public function fitlerStatusProduct($status);
}
