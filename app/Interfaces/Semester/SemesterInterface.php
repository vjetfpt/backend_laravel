<?php
namespace App\Interfaces\Semester;
use App\Interfaces\BaseInterface\BaseInterface;

interface SemesterInterface extends BaseInterface {
    public function getDataPangination();
}
