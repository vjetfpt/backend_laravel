<?php
namespace App\Interfaces\CommentInterface ;
use App\Interfaces\BaseInterface\BaseInterface;

interface CommentInterface extends BaseInterface {
     public function comment ($data);
     public function getComments();
     public function reply ($data);
     public function editComment ($data);
     public function editReply ($data);
     public function getCommentsOfProduct ($data);
     public function deleteComment ($data);
     public function deleteReply ($data);
     public function getNewComments(); 
}
