<?php
namespace App\Interfaces\Product;
use App\Interfaces\BaseInterface\BaseInterface;

interface ProductInterface extends BaseInterface {
    public function addMember($listStudent,$saveId);
    public function getInfo($request);
    public function updateProduct($data, $idProduct, $idUser);
    public function deleteProduct($product);
    function deleteImage($imageDb, $folder = 'products');
    public function deleteDocument($resource_url);
    public function getProduct($token);
    public function rating($data);
    public function avgStar($data);
    public function countStarInProduct($count);
}
