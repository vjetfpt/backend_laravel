<?php

namespace App\Providers;

use App\Interfaces\Campus\CampusInterface;
use App\Interfaces\Major\MajorInterface;
use App\Interfaces\Permission\PermissionInterface;
use App\Interfaces\Subject\SubjectInterface;
use App\Interfaces\ProductType\ProductTypeInterface;
use App\Repositories\ProductType\ProductTypeRepository;
use App\Interfaces\Semester\SemesterInterface;
use Illuminate\Support\ServiceProvider;
use App\Interfaces\User\UserInterface;
use App\Interfaces\CategorySubject\CategorySubjectInterface;
use App\Interfaces\Client\ClientInterface;
use App\Interfaces\CommentInterface\CommentInterface;
use App\Interfaces\FeedBack\FeedBackInterface;
use App\Interfaces\Filter\FilterInterface;
use App\Repositories\Major\MajorRepository;
use App\Repositories\Semester\SemesterRepository;
use App\Repositories\Subject\SubjectRepository;
use App\Repositories\User\UserRepository;
use App\Interfaces\Product\ProductInterface;
use App\Interfaces\Role\RoleInterface;
use App\Interfaces\ViewPermission\ViewPermissionInterface;
use App\Repositories\Campus\CampusRepository;
use App\Repositories\Product\ProductRepository;
use App\Models\PersonalAccessToken;
use App\Repositories\Permission\PermissionRepository;
use App\Repositories\Role\RoleRepository;
use Laravel\Sanctum\Sanctum;
use App\Repositories\CategorySubject\CategorySubjectRepository;
use App\Repositories\Client\ClientRepository;
use App\Repositories\CommentRepository\CommentRepository;
use App\Repositories\FeedBack\FeedBackRepository;
use App\Repositories\Filter\FilterRepository;
use App\Repositories\ViewPermission\ViewPermissionRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserInterface::class, UserRepository::class);
        $this->app->bind(MajorInterface::class, MajorRepository::class);
        $this->app->bind(SubjectInterface::class, SubjectRepository::class);
        $this->app->bind(ProductInterface::class, ProductRepository::class);
        $this->app->bind(ProductTypeInterface::class, ProductTypeRepository::class);
        $this->app->bind(SemesterInterface::class, SemesterRepository::class);
        $this->app->bind(CampusInterface::class, CampusRepository::class);
        $this->app->bind(PermissionInterface::class, PermissionRepository::class);
        $this->app->bind(RoleInterface::class, RoleRepository::class);
        $this->app->bind(CategorySubjectInterface::class, CategorySubjectRepository::class);
        $this->app->bind(ViewPermissionInterface::class, ViewPermissionRepository::class);
        $this->app->bind(CommentInterface::class, CommentRepository::class);
        $this->app->bind(FilterInterface::class, FilterRepository::class);
        $this->app->bind(FeedBackInterface::class, FeedBackRepository::class);
        $this->app->bind(ClientInterface::class, ClientRepository::class);

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Sanctum::usePersonalAccessTokenModel(PersonalAccessToken::class);
    }
}
