<?php

namespace App\Http\Libraries;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class HelperFunction
{

    public function saveImage($image, $folder, $file_name)
    {

//        $time = date('Y-m-d');
//        $folderYear = date('Y', strtotime($time));
//        $folderMonth = date('m', strtotime($time));
//        $sCurrentDay = date('d', strtotime($time));
//        . "$folderYear" . "/" . "$folderMonth" . "/" . "$sCurrentDay"
        $storage = Storage::disk('public');
        $path = "uploads/" . "$folder";
        $public_path = storage_path('app/public/' . $path . '/' . "$file_name");
        $checkDirectory = $storage->exists($path);
        if (!$checkDirectory) {
            $storage->makeDirectory($path);
        }
        Image::make($image)->save($public_path);
        return request()->getSchemeAndHttpHost() . '/storage/' . $path. '/'  . $file_name;
    }

    public function deleteImageBase64($fileName)
    {
        $fileName = \str_replace(request()->getSchemeAndHttpHost() . '/storage', '', $fileName);
        if ($fileName) {
            Storage::disk('public')->delete($fileName);
        }
    }
}
