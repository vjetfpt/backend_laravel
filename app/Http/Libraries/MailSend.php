<?php

namespace App\Http\Libraries;

class MailSend
{

    private $sender_name;

	private $template_dir;

	public function __construct() {
        $this->template_dir = config('config.mail.template_mail_dir');
        $this->sender_name = config('config.mail.sender_name');
	}

	public function createSendMailStudent($form, $options)
    {
        $email      = $options['email'];
        
        // Gui email
        $subject = "tiêu đề";

        $body = $this->setBody($form, array(
                'email'              => $options['email'],
            )
        );

        $body     = $this->headerTemplate($form).$body.$this->footerTemplate();

        $params = array(
            'email'       => $email
            ,'subject'    => $subject
            ,'body'       => $body
            ,'senderName' => $this->sender_name
        );

        
        $this->processMail($params);
        
    }

    
    private function headerTemplate($form)
    {
        $form = strtoupper($form);
        $form = str_replace(array('.HTML','.HTM'), '', $form);
        $html = '<div style="width:795px; height:44px;" align="right"><p style="text-align: right;font-family:\'Times New Roman\', Times, serif;font-size:12px;text-decoration:underline;}"></p></div>';
        return $html;
    }

    private function footerTemplate()
    {
        $html = '<img src = "https://ap.poly.edu.vn/images/logo.png"/>';
        return $html;
    }

	private function setBody($form, $options)
    {
        $templatePath = $this->template_dir . '/'. $form . '.html';
        $content      = file_get_contents($templatePath);
        $tags         = array_keys($options);
        foreach ($tags as $value) {
            $content  = str_replace($value, $options[$value], $content);
        }
        return $content;
    }

 
    public function processMail($arrParams) {
        \Mail::send([], [], function($message) use ($arrParams){
          $message->to($arrParams['email'])
            ->from("sonldp11925@fpt.edu.vn",$arrParams['senderName'])
            ->subject($arrParams['subject'])
            ->setBody($arrParams['body'], 'text/html');
        });
    }


}
