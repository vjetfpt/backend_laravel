<?php

namespace App\Http\Libraries;

class Mail
{

    private $sender_name;

    private $template_dir;

    public function __construct()
    {
        $this->template_dir = config('config.mail.template_mail_dir');
        $this->sender_name = config('config.mail.sender_name');
    }

    public function createSendMailStudent($form, $options)
    {
        $email = $options['email'];
        $name  = $options['name'];

        $subject = "Phòng đào tạo FPT Polytechnic";
        $body = $this->setBody(
            $form,
            [
                'email' => $email,
                'name'  => $name,
                'subject_code' => $options['subject_code'],
                'token' => $options['token'],
                'teacher'  => $options['teacher']
            ]
        );
        $body = $this->headerTemplate($form) . $body . $this->footerTemplate();
        $params = array(
            'email' => $email,
            'subject' => $subject,
            'body' => $body,
            'senderName' => $this->sender_name
        );
        $this->processMail($params);
    }
    public function createSendMailTeacher($form, $options)
    {
        $email = $options['email'];
        $name  = $options['name'];
        $id = $options['id'];
        $subject = "Phòng đào tạo FPT Polytechnic";
        $body = $this->setBody(
            $form,
            [
                'email' => $email,
                'name'  => $name,
                'id' => $id
            ]
        );
        $body = $this->headerTemplate($form) . $body;
        $params = array(
            'email' => $email,
            'subject' => $subject,
            'body' => $body,
            'senderName' => $this->sender_name
        );
        $this->processMail($params);
    }

    public function approveProductFail($form, $options){
        $email      = $options['email'];
        $name       = $options['name'];

        $subject = "Phòng đào tạo FPT Polytechnic";
        $body = $this->setBody(
            $form,
            [
                "email"  => $email,
                "name"   => $name,
                "id" => $options['id'],
                "teacher" => $options['teacher'],
                "subject_code" => $options['subject_code'],
                "subjectName" => $options['subject_name'],
                "message"     => $options['message'],
                "token"       => $options['token']
            ]
        );

        $body = $this->headerTemplate($form) . $body;
        $params = array(
            'email'      => $email,
            'subject'    => $subject,
            'body'       => $body,
            'senderName' => $this->sender_name
        );
        $this->processMail($params);
    }

    public function approveProductSuccess($form, $options){
        $email      = $options['email'];
        $name       = $options['name'];

        $subject = "Phòng đào tạo FPT Polytechnic";
        $body = $this->setBody(
            $form,
            [
                'email'  => $email,
                'name'   => $name,
                "teacher" => $options['teacher'],
                "subject_code" => $options['subject_code'],
                "id" => $options['id'],
                "subjectName" => $options['subject_name'],
            ]
        );
        $body = $this->headerTemplate($form) . $body;
        $params = array(
            'email'      => $email,
            'subject'    => $subject,
            'body'       => $body,
            'senderName' => $this->sender_name
        );
        $this->processMail($params);
    }


    public function chairmanApprovedProductFail($form, $options){
        $email      = $options['email'];
        $name       = $options['name'];
        $subject = "Phòng đào tạo FPT Polytechnic";
        $body = $this->setBody(
            $form,
            [
                "email"  => $email,
                "name"   => $name,
                "id" => $options['id'],
                "teacher" => $options['teacher'],
                "subject_code" => $options['subject_code'],
                "subjectName" => $options['subject_name'],
                "message"     => $options['message'],
                "token"       => $options['token']
            ]
        );
        $body = $this->headerTemplate($form) . $body;
        $params = array(
            'email'      => $email,
            'subject'    => $subject,
            'body'       => $body,
            'senderName' => $this->sender_name
        );
        $this->processMail($params);
    }

    public function chairmanApprovedProductSuccess($form, $options){
        $email      = $options['email'];
        $name       = $options['name'];
        $subject = "Phòng đào tạo FPT Polytechnic";
        $body = $this->setBody(
            $form,
            [
                "email"  => $email,
                "name"   => $name,
                "id" => $options['id'],
                "teacher" => $options['teacher'],
                "subject_code" => $options['subject_code'],
                "subjectName" => $options['subject_name'],
            ]
        );
        $body = $this->headerTemplate($form) . $body;
        $params = array(
            'email'      => $email,
            'subject'    => $subject,
            'body'       => $body,
            'senderName' => $this->sender_name
        );
        $this->processMail($params);
    }

    public function feedBack($form, $options){
        $email  = $options['email'];
        $name = $options['name'];
        $subject = "Sinh viên phản hồi";
        $body = $this->setBody(
            $form,
            [
                "email"  => $email,
                "name"   => $name
            ]
        );
        $body = $this->headerTemplate($form) . $body;
        $params = array(
            'email'      => $email,
            'subject'    => $subject,
            'body'       => $body,
            'senderName' => $this->sender_name
        );
        $this->processMail($params);
    }

    public function feedBackForAdmin($form, $options){
        $name  = $options['name'];
        $email  = config('config.mail.mail_receive_contact');
        $gmail_feedback = $options['email'];
        $content = $options['content'];
        $subject = "Sinh viên phản hồi";
        $body = $this->setBody(
            $form,
            [
                "gmail_feedback"  => $gmail_feedback,
                "name"  => $name,
                "content"  => $content
            ]
        );
        $body = $this->headerTemplate($form) . $body;
        $params = array(
            'email'      => $email,
            'subject'    => $subject,
            'body'       => $body,
            'senderName' => $this->sender_name
        );
        $this->processMail($params);
    }

    /*===================>METHOD SUPPORT<================*/
    private function headerTemplate($form)
    {
        $form = strtoupper($form);
        $form = str_replace(array('.HTML', '.HTM'), '', $form);
        $html = '<div style="width:795px; align="right"><p style="text-align: right;font-family:\'Times New Roman\', Times, serif;font-size:12px;text-decoration:underline;}"></p></div>';
        return $html;
    }

    private function footerTemplate()
    {
        $html = '<div>
        <p>Mọi thắc mắc các em liên hệ: mailadmin@fpt.edu.vn để được giải đáp</p>
        <p>Chúc các em học tập thật tốt!</p>
        <p>-------</p>
        <p>Trân trọng!</p>
        <p>LÊ MAI ADMIN (Ms.)</p>
        <p>Academic Staff</p>
    </div>
    <div>
        <img width="200px" src = "https://ap.poly.edu.vn/images/logo.png"/>
    </div>';
        return $html;
    }

    private function setBody($form, $options)
    {
        $templatePath = $this->template_dir . '/' . $form . '.html';
        $content = file_get_contents($templatePath);
        $tags = array_keys($options);
        foreach ($tags as $value) {
            $content = str_replace($value, $options[$value], $content);
        }
        return $content;
    }

    public function processMail($arrParams)
    {
        \Mail::send([], [], function ($message) use ($arrParams) {
            $message->to($arrParams['email'])
                ->from(env('MAIL_USERNAME', 'manhquana5yl@gmail.com'), $arrParams['senderName'])
                ->subject($arrParams['subject'])
                ->setBody($arrParams['body'], 'text/html');
        });
    }
}
