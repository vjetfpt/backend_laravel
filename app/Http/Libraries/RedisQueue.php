<?php

namespace App\Http\Libraries;

use Predis\Client;

class RedisQueue extends Client
{

    public function __construct($host = null, $port = null, $database = null)
    {
        if ($host && $port && $database) {
            $this->host = $host;
            $this->port = $port;
            $this->database = $database;
        } else {
            $this->host = config('database.redis.default.host');
            $this->port = config('database.redis.default.port');
            $this->database = config('database.redis.default.database');
        }
        parent::__construct([
            'scheme' => 'tcp',
            'host'   => $this->host,
            'port'   => $this->port,
        ], [
            'parameters' => [
                'database' => $this->database,
            ],
        ]);
    }

}
