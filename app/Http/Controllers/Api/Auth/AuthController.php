<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Interfaces\User\UserInterface;
use Illuminate\Support\Facades\Http;

class AuthController extends Controller
{
    protected $userRepository;

    public function __construct(UserInterface $userInterface)
    {
        $this->userRepository = $userInterface;
    }

    public function loginGoogleCallBack(Request $request)
    {
        $userSocial = Socialite::driver('google')->stateless()->userFromToken($request->access_token);
        $user = User::where("email", $userSocial->getEmail())->first()->load(['majors']);
        if($user->campuses->code != $request->campus_code){
            return response()->json(['errors' => 'Tài khoản không thuộc cơ sở'], 401);
        }
        $user->load('role.permission.viewPermission');
        $check_role_login = $user->roles->pluck('name')->toArray();
        if(empty($check_role_login)){
            return response()->json(['errors' => 'Cảnh báo không có quyền truy cập vào hệ thống'], 401);
        }
        $token = $this->supportLoginCallBack($userSocial);
        $time_token = config('sanctum.expiration');
        if ($token) {
            return response()->json([
                'success' => 'Đăng nhập thành công', 
                'access_token'  => $token,
                'time_token' => $time_token,
                'user' => $user
            ]);

        }else{
            return response()->json([
                'msg' => 'Đăng nhập không thành công', 
            ]);
        }
    }

    private function supportLoginCallBack($login)
    {
        $nameCode  = rtrim($login->getEmail(), "@fpt.edu.vn");
        if (count(array_filter(str_split($nameCode), 'is_numeric')) > 4) {
            $codeName = strrev($nameCode);
            $studentCode = substr($codeName, 0, 7);
            $codeStudent = strrev($studentCode);
        } else {
            $codeStudent = null;
        }
        $user = User::where("email", $login->getEmail())->first();
        if ($user) {
            $update = [
                "google_id" => $login->getId()
            ];
            if (!$user->avatar) {
                $update['avatar'] = $login->getAvatar();
            }
            if (!$user->name) {
                $update['name'] = $login->getName();
            }
            if (!$user->student_code) {
                $update['student_code'] = strtoupper($codeStudent);
            }
            $userUpate = $this->userRepository->update($user->id, $update);
            $token = $userUpate->createToken("PassportExample")->plainTextToken;
            return $token;
        }
        return false;
    }

    public function loginUser(Request $request){
        $userSocial = Socialite::driver('google')->stateless()->userFromToken($request->access_token);
        $token = $this->supportLoginUser($userSocial);
        $user = User::where("email", $userSocial->getEmail())->first();
        $time_token = config('sanctum.expiration');
        if ($token) {
            return response()->json([
                'success' => 'Đăng nhập thành công', 
                'access_token'  => $token,
                'time_token' => $time_token,
                'user' => $user
            ]);

        }else{
            return response()->json([
                'msg' => 'Đăng nhập không thành công', 
            ]);
        }
    }
    
    private function supportLoginUser($login){
        $nameCode  = rtrim($login->getEmail(), "@fpt.edu.vn");
        if (count(array_filter(str_split($nameCode), 'is_numeric')) > 4) {
            $codeName = strrev($nameCode);
            $studentCode = substr($codeName, 0, 7);
            $codeStudent = strrev($studentCode);
        } else {
            $codeStudent = null;
        }
        $user = User::where("email", $login->getEmail())->first();
        if ($user) {
            $update = [
                "google_id" => $login->getId()
            ];
            if (!$user->avatar) {
                $update['avatar'] = $login->getAvatar();
            }
            if (!$user->name) {
                $update['name'] = $login->getName();
            }
            if (!$user->student_code) {
                $update['student_code'] = strtoupper($codeStudent);
            }
            $userUpate = $this->userRepository->update($user->id, $update);
            $token = $userUpate->createToken("PassportExample")->plainTextToken;
            return $token;
        }else{
            $data_create = [
                "google_id" => $login->getId(),
                "avatar" => $login->getAvatar(),
                "name" => $login->getName(),
                "student_code" => $codeStudent,
                "email" => $login->getEmail(),
                "is_active" => 1
            ];
            $userUpate = $this->userRepository->create($data_create);
            $token = $userUpate->createToken("PassportExample")->plainTextToken;
            return $token;
        }
    }

    public function refresh(Request $request)
    {
        $user = $request->user();
        $user->load('role.permission.viewPermission');
        $request->user()->tokens()->delete();
        $time_token = config('sanctum.expiration');
        return response()->json([
            'access_token' => $request->user()->createToken('refreshToken')->plainTextToken,
            'time_token' => $time_token,
            'user' => $user
        ]);
    }

    public function logout()
    {
        $user = Auth::user();
        $user->tokens()->delete();
        return response()->json(['success' => 'Đăng xuất thành công'], 200);
    }

}
