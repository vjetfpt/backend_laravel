<?php

namespace App\Http\Controllers\Api\Comment;

use App\Http\Controllers\Controller;
use App\Interfaces\CommentInterface\CommentInterface;
use App\Models\Comment;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    protected $commentRepository;
    public function __construct(CommentInterface $commentRepository)
    {
        $this->commentRepository = $commentRepository;
    }

    public function comment(Request $request)
    {
        $data = $request->all();
        $data['parent_id'] = 0;
        $data['user_id'] = Auth::id();
        $comment = $this->commentRepository->comment($data);
        if ($comment == false) {
            return response()->json(['message' => 'Bình luận không hợp lệ, vui lòng có văn hóa :)']);
        }
        return response()->json(['data' => $comment],200);
    }

    public function getComments()
    {
        if(request()->page != null && request()->pageLength != null){
            $page = request()->page;
            $pagelength = request()->pageLength;
            $get_comments = $this->commentRepository->getDataPaginate($page, $pagelength);
        }
        else{
            $get_comments =  $this->commentRepository->getAll();
        }
        $get_comments->load('getInfoUser', 'getInfoProduct');
        return response()->json([
            'data' => $get_comments,
            'total' => $this->commentRepository->countRecord()
        ], 200);
    }

    public function reply(Request $request, Comment $comment)
    {
        $data = $request->all();
        $data['comment_id'] = $comment->id;
        $data['user_id'] = Auth::id();
        $reply = $this->commentRepository->reply($data);
        if($reply == false) {
            return response()->json(['message' => 'Bình luận không hợp lệ']);
        }
        return response()->json(['reply' => $reply],200);
    }

    public function editComment(Request $request, Comment $comment)
    {
        $data = $request->all();
        $data['comment_id'] = $comment->id;
        $data['user_id'] = Auth::id();
        $edit = $this->commentRepository->editComment($data);

        if($edit == false) {
            return response()->json(['message' => 'Bình luận không hợp lệ']);
        }
        return response()->json(['message' => 'Chỉnh sửa comment thành công' ,'data' => $edit ],200);
    }

    public function editReply(Request $request, Comment $comment)
    {
        $data = $request->all();
        $data['user_id'] = Auth::id();
        $data['comment_id'] = $comment->id;
        $edit_reply = $this->commentRepository->editReply($data);
        if ($edit_reply == false) {
            return response()->json(['message' => 'Bình luận không hợp lệ']);
        }
        return response()->json(['message' => 'Chỉnh sửa bình luận thành công', 'data' => $edit_reply],200);
    }

    public function getCommentsOfProduct(Product $product)
    {
        $data['product_id'] = $product->id;
        $comment_of_product = $this->commentRepository->getCommentsOfProduct($data);
        return response()->json(['data' => $comment_of_product]);
    }

    public function deleteComment(Comment $comment)
    {
        $data['comment_id'] = $comment->id;
        $data['user_id'] = Auth::id();
        $delete = $this->commentRepository->deleteComment($data);
        if ($delete == false) {
            return response()->json(['message' => 'Xóa comment không thành công']);
        }
        return response()->json(['message' => 'Đã xóa comment'],200);
    }

    public function deleteReply(Comment $comment)
    {
        $data['user_id'] = Auth::id();
        $data['comment_id'] = $comment->id;
        $delete_reply = $this->commentRepository->deleteReply($data);
        return response()->json(['message' => 'Xóa comment thành công'],200);
    }

    public function getNewComment ()
    {
        $new_comments = $this->commentRepository->getNewComments();
        return response()->json(['data' => $new_comments],200);
    }
}
