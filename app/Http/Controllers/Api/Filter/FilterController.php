<?php

namespace App\Http\Controllers\Api\Filter;

use App\Http\Controllers\Controller;
use App\Interfaces\Filter\FilterInterface;
use Illuminate\Http\Request;

class FilterController extends Controller
{
    protected $filterProductRepository;

    public function __construct(FilterInterface $filterInterface)
    {
        $this->filterProductRepository = $filterInterface;
    }

    public function searchProduct(Request $request)
    {
        $text = $request->input('text');
        if(empty($text)){
            return response()->json([
                'status' => 'Dữ liệu gửi lên không được để trống'
            ]);
        }
        else{
            $listProduct = null;
            if(preg_match('/^(PH)?[0-9]+$/i', $text)){
                $listProduct = $this->filterProductRepository->searchProductStudentId($text);
            }
            else{
                $listProduct = $this->filterProductRepository->searchProductName($text);
            }
            $status = $listProduct != null ? 'Tồn tại dữ liệu.': 'Không tồn tại dữ liệu.';
            return response()->json([
                'status' => $status,
                'data' => $listProduct
            ]);
        }
    }

    public function filter(Request $request){
        $id = $request->input('id');
        $type = $request->input('type');
        return response()->json(
            [
                'data' => $this->filterProductRepository->filterId($id,$type)
            ]
        );
    }

    public function filterStatusProduct($status){
        if(in_array($status,config('common.product_status'))){
            return response()->json(
                [
                    'data' => $this->filterProductRepository
                        ->fitlerStatusProduct($status)
                ]
            );
        }
        else{
            return response()->json([
                'message' => 'status truyền lên không hợp lệ.'
            ]);
        }
    }
    
}
