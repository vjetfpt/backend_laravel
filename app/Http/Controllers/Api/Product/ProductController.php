<?php

namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\Controller;
use App\Http\Libraries\HelperFunction;
use App\Http\Libraries\Mail;
use App\Http\Requests\Product\ProductApproveRequest;
use App\Interfaces\Product\ProductInterface;
use App\Models\Product;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\Product\StoreProductRequest;
use Illuminate\Http\Request;
use App\Http\Requests\Product\StoreGalleriesRequest;
use App\Http\Requests\Product\StoreDocumentRequest;
use App\Http\Requests\Product\ApproveProductRequest;
use App\Http\Requests\Product\StorageInfoRequest;
use App\Http\Requests\Product\StorageUploadImage;
use App\Http\Requests\Product\StorageUploadVideo;
use App\Http\Requests\Product\UpdateProductRequest;
use App\Interfaces\Client\ClientInterface;
use App\Interfaces\Subject\SubjectInterface;
use App\Interfaces\User\UserInterface;
use App\Models\StudentGroup;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Vimeo\Laravel\VimeoManager;
use Str;
use Illuminate\Support\Str as Strs;

class ProductController extends Controller
{
    protected $productRepository;
    protected $helperFunction;
    protected $mail;
    protected $userRepository;
    protected $subjectRepository;
    protected $clientInterface;

    public function __construct(
        ProductInterface $productInterface,
        HelperFunction $helperFunction,
        Mail $mail,
        UserInterface $userInterface,
        SubjectInterface $subjectInterface,
        ClientInterface $clientInterface
    ) {
        $this->productRepository = $productInterface;
        $this->helperFunction = $helperFunction;
        $this->mail = $mail;
        $this->userRepository = $userInterface;
        $this->subjectRepository = $subjectInterface;
        $this->clientRepository = $clientInterface;
    }

    public function getAll()
    {
        if (request()->page != null && request()->pageLength != null) {
            $page = request()->page;
            $pagelength = request()->pageLength;
            $products = $this->productRepository->getDataPaginate($page, $pagelength);
        } else {
            $products = $this->productRepository->getAll();
        }
        $products = !empty($products) ? $products->load(['teacher', 'subject', 'students', 'productGalleries'])
            : [];
        $products = $products->toArray();
        for ($i = 0; $i < count($products); $i++) {
            if ($products[$i]['status'] < 3) {
                unset($products[$i]);
            } else {
                if ($products[$i]['subject']) {
                    $masterUser = $this->subjectRepository->findChairmanMajor($products[$i]['subject']['major_id']);
                    $products[$i]['master_user'] = $masterUser != null ? $masterUser->toArray() : null;
                }
            }
        }
        $listProduct = [];
        if(!empty($products)){
            foreach($products as $item){
                $listProduct[] = $item;
            }
        }
        return response()->json([
            'message' => 'Lấy dữ liệu sản phẩm thành công',
            'total' => count($products),
            'data' => $listProduct,
        ], 200);
    }

    public function getById(Product $product)
    {
        $product = $this->productRepository->find($product->id);
        $product->load(['students', 'semester', 'teacher', 'productGalleries', 'major', 'subject']);
        $productRelated = $this->clientRepository->getProductMajor($product->major->id, 1, 5) ?? [];
        $listProductRelated = [];
        if (!empty($productRelated)) {
            $arrProductRelated = $productRelated->whereNotIn('id', $product->id);
            foreach ($arrProductRelated as $item) {
                $listProductRelated[] = $item;
            }
        }
        $star = DB::table('ratings')->where('rateable_id', $product->id)->where('user_id', Auth::id())->select('rating')->first();
        return response()->json([
            'message' => 'Thông tin product',
            'data' => $product, 'star' => $star,
            'product_related' => $listProductRelated
        ], 200);
    }

    public function updateFirst(StoreProductRequest $request)
    {
        try {
            DB::beginTransaction();
            $data = $request->only([
                'class', 'product_type_id', 'status',
                'video_url', 'description', 'name', 'token'
            ]);
            $data['resource_url'] = $request->resource_url;
            $data['image'] = $request->image_url;
            // $this->productRepository->setStatusTranscript($request->input('email'), $data['subject_id']);
            $product = $this->productRepository->getProduct($data['token']);
            $data['user_id'] = Auth::id();
            $data['token'] = '';
            $save = $this->productRepository->update($product->id, $data);
            $saveId = $product->id;
            $galleries = $request->galleries;
            $array_url = [];
            foreach ($galleries as $item) {
                $array_url['image_url'] = $item;
                $array_url['product_id'] = $saveId;
                $array_insert[] = $array_url;
            }
            DB::table('product_galleries')->insert($array_insert);
            $listStudent = $request->students;
            $this->productRepository->addMember($listStudent, $saveId);
            $userTeacher = DB::table('users')->where('id', $product->teacher_id)->first();
            $this->mail->createSendMailTeacher('SEND_MAIL_TEACHER', [
                'email' => $userTeacher->email,
                'name' => $userTeacher->name,
                'id' => $saveId
            ]);
            $save->load(['productGalleries', 'students', 'teacher']);
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
        return $save;
    }

    public function uploadMultipleImage(StoreGalleriesRequest $request)
    {
        $galleries = $request->file('galleries');
        foreach ($galleries as $item) {
            $file_gallery_name = \Str::uuid() . "-" . $item->getClientOriginalName();
            $url = $this->helperFunction->saveImage($item, 'product_galleries', $file_gallery_name);
            $array_insert[] = $url;
        }
        return response()->json(['array_url' => $array_insert]);
    }

    public function uploadDocument(StoreDocumentRequest $request)
    {
        $file_document_upload = $request->resource_url;
        $file_document_name = $file_document_upload->getClientOriginalName();
        $storage = Storage::disk('google');
        $name_folder_make = rand(0, 9999999) . '-' . $request->name;
        $storage->makeDirectory($name_folder_make);
        $info = collect($storage->listContents('/', false))->where('type', 'dir')
            ->where('name', $name_folder_make)
            ->first();
        $root = 'https://drive.google.com/drive/folders/1Kke9PWsvU0VmoUi7RjMR01b8Y2pGQUkK/';
        $url_put = $root . $info['path'];
        $file = $storage->putFileAs($url_put, $file_document_upload, $file_document_name);
        $url_folder =  substr($file, 0, strrpos($file, "/"));
        return response()->json(['resource_url' => $url_folder]);
    }


    public function approveTeacher($id, ApproveProductRequest $request)
    {
        $user_login_in = Auth::user();
        $product = $this->productRepository->find($id);
        $dataUpdate['status'] = $request->status;
        $dataUpdate['token'] = '';
        if ($user_login_in->id != $product->teacher_id) {
            return response()->json(['errors' => 'Bạn không có quyền phê duyệt sản phẩm này'], 401);
        }
        if ($dataUpdate['status'] == 0) {
            $productUpdate = $this->productRepository->update($id, $dataUpdate);
            $productUpdate->load('students');
            $student = $this->userRepository->find($productUpdate->user_id);
            $this->mail->approveProductFail('TEACHER_APPROVE_PRODUCT', [
                'email' => $student->email,
                'name'  => $student->name,
                'id'    => $productUpdate->id,
                'teacher' => $productUpdate->teacher->email,
                'subject_code' => $productUpdate->subject->code,
                'subject_name' => $productUpdate->subject->name,
                'message'      => $request->message,
                'token'        => $productUpdate->token
            ]);
            return response()->json([
                'success' => 'Phê duyệt sản phẩm không đạt yêu cầu',
                'data'    => $productUpdate
            ], 200);
        } else {
            $productUpdate = $this->productRepository->update($id, $dataUpdate);
            $productUpdate->load('students');
            $subject = $this->subjectRepository->find($productUpdate->subject_id);
            $userMaster = $this->subjectRepository->findChairmanMajor($subject->major_id);
            if ($userMaster == null) {
                return response()->json([
                    'message' => 'Không tồn tại chủ nhiệm bộ môn này'
                ]);
            } else {
                $this->mail->approveProductSuccess('TEACHER_APPROVE_SUCCESS_PRODUCT', [
                    'email' => $userMaster->email,
                    'name'  => $userMaster->name,
                    'teacher' => $productUpdate->teacher->name,
                    'subject_code' => $productUpdate->subject->code,
                    'subject_name' => $productUpdate->subject->name,
                    'id' => $productUpdate->id
                ]);
                return response()->json([
                    'success' => 'Phê duyệt lần 1 sản phẩm thành công',
                    'data'    => $productUpdate
                ], 200);
            }
        }
    }


    public function uploadImage(StorageUploadImage $request)
    {
        $file_image = $request->image;
        $file_image_name = \Str::uuid() . "-" . $file_image->getClientOriginalName();
        $url = $this->helperFunction->saveImage($file_image, 'products', $file_image_name);
        return response()->json(['image_url' => $url]);
    }

    public function getInfo()
    {
        $data = $this->productRepository->getInfo(request()->all());
        if (array_key_exists('message', $data)) {
            return response()->json($data, 404);
        }
        return response()->json($data, 200);
    }

    public function update(UpdateProductRequest $request, Product $product)
    {
        $data = $request->only([
            'class', 'product_type_id', 'video_url', 'description', 'status',
            'name', 'resource_url', 'image_url', 'galleries', 'students'
        ]);
        $dataResponse = $this->productRepository->updateProduct($data, $product, Auth::id());
        $userTeacher = DB::table('users')->where('id', $product->teacher_id)->first();
        $this->mail->createSendMailTeacher('SEND_MAIL_TEACHER', [
            'email' => $userTeacher->email,
            'name' => $userTeacher->name,
            'id' => $product->id
        ]);
        return response()->json($dataResponse);
    }

    public function uploadVideo(StorageUploadVideo $request, VimeoManager $vimeo)
    {
        $uri = $vimeo->upload(
            $request->video,
            [
                'name' => "$request->name",
                'description' => "Miêu tả $request->name"
            ]
        );
        $responseLink = $vimeo->request($uri . '?fields=link');
        return response()->json(['link_video' => $responseLink['body']['link']]);
    }

    public function delete(Product $product)
    {
        $check = $this->productRepository->deleteProduct($product);
        if ($check) {
            return response()->json([
                'status' => 'Delete success',
                'info' => $product->toArray()
            ]);
        } else {
            return response()->json(['status' => 'Delete failed']);
        }
    }

    public function createImageEdtior(Request $request)
    {
        $url = $request->url_image;
        $storage = Storage::disk('public');
        $path = 'uploads/images_editor';
        if (!$storage->exists($path)) {
            $storage->makeDirectory($path);
        }
        $contents = file_get_contents($url);
        $name = substr($url, strrpos($url, '/') + 1);
        $linkImage = $path . "/$name";
        $storage->put($linkImage, $contents);
        return response()->json(['link_image' => $request->getSchemeAndHttpHost() . '/storage/' . $linkImage]);
    }

    public function deleteImage(Request $request)
    {
        $this->productRepository->deleteImage($request->img_url, $request->folder);
        return response()->json(['status' => "Deleted image in $request->folder"]);
    }

    public function deleteDocument(Request $request)
    {
        $this->productRepository->deleteDocument($request->resource_url);
        return response()->json(['status' => "Deleted document"]);
    }

    public function downloadDocument(Request $request)
    {
        $url = $request->link;
        $index = strrpos($url, '/');
        $folderId = substr($url, $index + 1);
        $googleDriveStorage = Storage::disk('google');
        $fileinfo = collect($googleDriveStorage->listContents("/$folderId", false))->first();
        // dd($fileinfo) ;
        if ($fileinfo) {
            $path_folder = $fileinfo['path'];
            $path_file = strrpos($path_folder, '/');
            $get_path = substr($path_folder, $path_file + 1);
            $contents = $googleDriveStorage->get($get_path);
            return response($contents)
                ->header('Content-Type', $fileinfo['mimetype'])
                ->header('Content-Disposition', "attachment; filename=" . $fileinfo['name'] . "");
        } else {
            return response()->json(['message' => 'Không tồn tại file']);
        }
    }

    public function getProductOfUser($id)
    {
        $listProduct = DB::table('products')->where('user_id', $id)
            ->get()->toArray();
        return response()->json($listProduct);
    }

    public function chairmanApproved(ApproveProductRequest $request, Product $product)
    {
        $user_login_in = Auth::user();
        $data_update['status'] = $request->status;
        $subject = $this->subjectRepository->find($product->subject_id);
        $user_chairman = $this->subjectRepository->findChairmanMajor($subject->major_id);
        if ($user_chairman->id != $user_login_in->id) {
            return response()->json(['errors' => 'Bạn không có quyền phê duyệt sản phẩm này'], 401);
        }
        $product_updated  = $this->productRepository->update($product->id, $data_update);
        $product_updated->load('students');
        $student = $this->userRepository->find($product_updated->user_id);
        if ($data_update['status'] == 0) {
            $this->mail->chairmanApprovedProductFail('CHAIRMAIN_APPROVE_PRODUCT_FAIL', [
                'email' => $student->email,
                'name'  => $student->name,
                'id'    => $product_updated->id,
                'teacher' => $product_updated->teacher->email,
                'subject_code' => $product_updated->subject->code,
                'subject_name' => $product_updated->subject->name,
                'message'      => $request->message,
                'token'        => $product_updated->token
            ]);
            return response()->json([
                'success' => 'Phê duyệt sản phẩm không đạt yêu cầu',
                'data'    => $product_updated
            ], 200);
        } elseif ($data_update['status'] == 3) {
            $this->mail->chairmanApprovedProductSuccess('CHAIRMAIN_APPROVE_PRODUCT_SUCCESS', [
                'email' => $student->email,
                'name'  => $student->name,
                'id'    => $product_updated->id,
                'teacher' => $product_updated->teacher->email,
                'subject_code' => $product_updated->subject->code,
                'subject_name' => $product_updated->subject->name,
            ]);
            return response()->json([
                'success' => 'Phê duyệt thành công',
                'data'    => $product_updated
            ], 200);
        }
    }

    public function rating(Request $request, Product $product)
    {
        $data = $request->all();
        $data['user_id'] = Auth::id();
        $data['product_id'] = $product->id;
        $rating = $this->productRepository->rating($data);
        return response(['message' => 'Đã đánh giá'], 200);
    }

    public function avgStar(Product $product)
    {
        $data = [];
        $data['product_id'] = $product->id;
        $avgStar = $this->productRepository->avgStar($data);
        return response(['avgStar' => $avgStar], 200);
    }

    public function countStarInProduct(Product $product)
    {
        $data = [];
        $data['product_id'] = $product->id;
        $count = $this->productRepository->countStarInProduct($data);
        return response(['data' => $count]);
    }

    /**
     * @param Int $id;
     */
    public function productOfUser ($id)
    {
        $productOfUser = User::where('id', $id)->first();
        $productOfUser->load("getProducts");
        return response()->json(['data' => $productOfUser]);
    }
}
