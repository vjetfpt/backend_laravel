<?php

namespace App\Http\Controllers\Api\Subject;

use App\Http\Controllers\Controller;
use App\Http\Requests\Subject\SubjectRequest;
use App\Http\Requests\Subject\UpdateSubjectRequest;
use App\Interfaces\Major\MajorInterface;
use App\Interfaces\Subject\SubjectInterface;
use App\Models\Subject;

class SubjectController extends Controller
{
    protected $subjectRepository;
    protected $majorRepository;

    public function __construct(SubjectInterface $subjectInterface, MajorInterface $majorInterface)
    {
        $this->subjectRepository = $subjectInterface;
        $this->majorRepository = $majorInterface;

    }

    public function getAll()
    {
        if(request()->page != null && request()->pageLength != null){
            $page = request()->page;
            $pagelength = request()->pageLength;
            $subjects = $this->subjectRepository->getDataPaginate($page, $pagelength);
        }else{
            $subjects = $this->subjectRepository->getAll();
        }
        $subjects = !empty($subjects) ? $subjects->load('majors')->sortByDesc('created_at'): [];
        $listSubject = [];
        if(!empty($subjects)){
            foreach($subjects as $item){
                $listSubject[] = $item;
            }
        }
        return response()->json([
            'data' => $listSubject,
            'total' => $this->subjectRepository->countRecord()
        ], 200,array('Content-Type'=>'application/json; charset=utf-8' ));
    }


    public function find(Subject $subject)
    {
        $subject = $this->subjectRepository->find($subject->id);
        $subject->load('majors');
        return response()->json([
            'data' => $subject
        ], 200);
    }

    public function create(SubjectRequest $request)
    {
        $major = $this->majorRepository->find($request->major_id);
        if (!$major) {
            return response()->json([
                'error' => 'Không tìm thấy chuyên ngành'
            ], 404);
        }
        $subject = $this->subjectRepository->create($request->all());
        $subject->load('majors');
        return response()->json([
            'success' => 'Thêm môn học thành công',
            'data'    => $subject
        ], 200);
    }


    public function update(Subject $subject, UpdateSubjectRequest $request)
    {
        $major = $this->majorRepository->find($request->major_id);
        if (!$major) {
            return response()->json([
                'error' => 'Không tìm thấy chuyên ngành'
            ], 404);
        }
        $subjects = $this->subjectRepository->update($subject->id, $request->all());
        $subjects->load('majors');
        return response()->json([
            'success' => 'Cập nhật môn học thành công',
            'data'    => $subjects
        ], 200);
    }

    public function delete(Subject $subject)
    {
        $this->subjectRepository->delete($subject->id);
        return response()->json([
            'success' => 'Xóa môn học thành công'
        ], 200);
    }

}
