<?php

namespace App\Http\Controllers\Api\Campus;

use App\Http\Controllers\Controller;
use App\Interfaces\Campus\CampusInterface;
use Illuminate\Http\Request;

class CampusController extends Controller
{
    protected $campusRepository;

    public function __construct(CampusInterface $campusInterface)
    {
        $this->campusRepository = $campusInterface;
    }

    public function index(){
        $campuses = $this->campusRepository->getAll();
        return response()->json([
            'campuses' => $campuses
        ], 200);
    }
}
