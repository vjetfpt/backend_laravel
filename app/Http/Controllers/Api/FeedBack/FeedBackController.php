<?php

namespace App\Http\Controllers\Api\FeedBack;

use App\Http\Controllers\Controller;
use App\Http\Libraries\Mail;
use App\Interfaces\FeedBack\FeedBackInterface;
use App\Models\Feedbacks;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FeedBackController extends Controller
{
    protected $feedBackRepository;

    public function __construct(
        FeedBackInterface $feedBackInterface,
        Mail $mail
    ){
        $this->feedBackRepository = $feedBackInterface;
        $this->mail = $mail;
    }

    public function getAll()
    {
        if(request()->page != null && request()->pageLength != null){
            $page = request()->page;
            $pagelength = request()->pageLength;
            $feed_back = $this->feedBackRepository->getDataPaginate($page, $pagelength);
        }
        else{
            $feed_back =  $this->feedBackRepository->getAll();
        }
        return response()->json([
            'data' => $feed_back,
            'total' => $this->feedBackRepository->countRecord()
        ], 200);
    }

    public function find(Feedbacks $feedbacks)
    {
        $this->feedBackRepository->find($feedbacks->id);
        return response()->json([
            'data' => $feedbacks
        ], 200);
    }

    public function create(Request $request)
    {
        $feedback = $this->feedBackRepository->create($request->all());
        $this->mail->feedBack('FEED_BACK', [
            'name' =>  $feedback->name,
            'email' =>  $feedback->email,
        ]);
        $this->mail->feedBackForAdmin('FEED_BACK_FOR_ADMIN', [
            'name' =>  $feedback->name,
            'email' =>  $feedback->email,
            'content' =>  $feedback->content,
        ]);
        return response()->json([
            'success' => 'Gửi phản hồi thành công',
            'data'   => $feedback
        ]);
    }
    
    public function delete(Feedbacks $feedbacks)
    {
        $this->feedBackRepository->delete($feedbacks->id);
        return response()->json([
            'success' => 'Xóa phản hồi thành công'
        ], 200);
    }
}
