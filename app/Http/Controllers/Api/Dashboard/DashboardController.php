<?php

namespace App\Http\Controllers\Api\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\Feedbacks;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function getAll(){
        $products = Product::where('campus_id', Auth::user()->campus_id)->where('status', config('common.product_status.success'))->count();
        $product_not_approved_yet = Product::where('campus_id', Auth::user()->campus_id)->where('status' , '=', 1)->orWhere('status', '=', 2)->count();
        $product_student_has_not_posted = Product::where('campus_id', Auth::user()->campus_id)->where('status' , '=', 0)->count();
       
        $total_users = User::where('campus_id', Auth::user()->campus_id)->count();
        $total_comments = Comment::all()->count();
        $month_anuary = date('01');
        $month_april = date('04');
        $month_august = date('08');
        $month_december = date('12');
        $date1 = date('Y') . '-'. $month_anuary. date('-1 23:59:59');
        $date4 = date('Y') . '-'. $month_april. date('-30 23:59:59');
        $date8 = date('Y') . '-'. $month_august. date('-1 23:59:59');
        $date12 = date('Y') . '-'. $month_december. date('-31 23:59:59');
        $product['anuary_april'] = Product::whereBetween('created_at', [$date1, $date4])->get()->count();
        $product['april_august'] = Product::whereBetween('created_at', [$date4, $date8])->get()->count();
        $product['august_december'] = Product::whereBetween('created_at', [$date8, $date12])->get()->count();
        $feedback = Feedbacks::all()->sortByDesc('created_at');
        return response()->json([
            'success' => 'Thống kê dữ liệu',
            'total_products' => $products,
            'total_product_not_approved_yet' => $product_not_approved_yet,
            'total_product_student_has_not_posted' => $product_student_has_not_posted,
            'total_users' => $total_users,
            'total_comments' => $total_comments,
            'data' => $product,
            'feedback' => $feedback
        ], 200);
    }
}
