<?php

namespace App\Http\Controllers\Api\Role;

use App\Http\Controllers\Controller;
use App\Interfaces\Role\RoleInterface;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    protected $roleRepository;

    public function __construct(RoleInterface $roleInterface)
    {
        $this->roleRepository = $roleInterface;
    }

    public function getAll(){
        $roles = $this->roleRepository->getAll();
        $roles->load('permissions');

        return response()->json([
            'success' => 'Danh sách vài trò',
            'total'   => count($roles),
            'data'    => $roles
        ], 200);
    }

    public function getById(Role $role){
        $roles = $this->roleRepository->find($role->id);
        $roles->load('permissions');
        return response()->json(['data' => $roles] ,200);
    }

    public function create(Request $request){
        $data = $request->get('roles', []);
        $data_role = $data;
        if($data_role['permissions']){
            unset($data_role['permissions']);
        }
        $role = $this->roleRepository->create($data_role);
        if($data['permissions']){
            $role->givePermissionTo($data['permissions']);
        }
        return response()->json([
            'success' => 'Thêm mới vai trò thành công',
            'data' => $role
        ]);
    }

    public function update(Role $role, Request $request){
        $data = $request->get('roles', []);
        $role = $this->roleRepository->update($role->id, $data);
        if($data['permissions']){
            $role->syncPermissions($data['permissions']);
        }
        return response()->json([
            'success' => 'Cập nhật vai trò thành công',
            'data' => $role
        ]);
    }

    public function delete(Role $role){
        $this->roleRepository->delete($role->id);
        return response()->json(['success' => 'Xóa vai trò thành công']);
    }

}
