<?php

namespace App\Http\Controllers\Api\Semester;

use App\Http\Controllers\Controller;
use App\Interfaces\Semester\SemesterInterface;
use App\Http\Requests\Semester\StoreSemesterRequestAdd;
use App\Http\Requests\Semester\StoreSemesterRequestUpdate;
use App\Models\Semester;

class SemesterController extends Controller
{
    protected $semesterRepository;

    public function __construct(SemesterInterface $semesterInterface)
    {
        $this->semesterRepository = $semesterInterface;
    }

    public function index()
    {
        if (request()->page != null && request()->pageLength != null) {
            $page = request()->page;
            $pagelength = request()->pageLength;
            $semesters = $this->semesterRepository->getDataPaginate($page, $pagelength);
        } else {
            $semesters = $this->semesterRepository->getAll();
        }

        return response()->json([
            'semesters' => $semesters,
            'total' => $this->semesterRepository->countRecord()
        ], 200);
    }

    public function paginate()
    {
        $semesters = $this->semesterRepository->getDataPangination();
        return response()->json(['semesters' => $semesters], 200);
    }

    public function store(StoreSemesterRequestAdd $request)
    {
        $semester = $this->semesterRepository->create($request->all());
        return response()->json($semester, 201);
    }

    public function show($id)
    {
        $semester = $this->semesterRepository->find($id);
        return response()->json(['semester' => $semester], 200);
    }

    public function update(StoreSemesterRequestUpdate $request, Semester $semester)
    {
        $semester = $this->semesterRepository->update($semester->id, $request->all());
        return response()->json([$semester], 200);
    }

    public function destroy($id)
    {
        $semester = $this->semesterRepository->delete($id);
        return response()->json([$semester], 204);
    }
}
