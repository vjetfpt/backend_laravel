<?php

namespace App\Http\Controllers\Api\Major;

use App\Http\Controllers\Controller;
use App\Http\Requests\Major\MajorRequest;
use App\Http\Requests\Major\UpdateMajorRequest;
use App\Interfaces\Major\MajorInterface;
use App\Models\Major;
class MajorController extends Controller
{
    protected $majorRepository;

    public function __construct(MajorInterface $majorInterface)
    {
        $this->majorRepository = $majorInterface;
    }

    public function getAll()
    {
        if(request()->page != null && request()->pageLength != null){
            $page = request()->page;
            $pagelength = request()->pageLength;
            $majors = $this->majorRepository->getDataPaginate($page, $pagelength);
        }
        else{
            $majors =  $this->majorRepository->getAll();
        }
        return response()->json([
            'data' => $majors,
            'total' => $this->majorRepository->countRecord()
        ], 200);
    }

    public function find(Major $major)
    {
        $this->majorRepository->find($major->id);

        return response()->json([
            'data' => $major
        ], 200);
    }

    public function create(MajorRequest $request)
    {
        $major = $this->majorRepository->create($request->all());
        return response()->json([
            'success' => 'Thêm chuyên ngành thành công',
            'data'   => $major
        ]);
    }

    public function update(Major $major, UpdateMajorRequest $request)
    {
        $major = $this->majorRepository->update($major->id, $request->all());
        return response()->json([
            'success' => 'Cập nhật chuyên ngành thành công',
            'data'   => $major
        ], 200);
    }

    public function delete(Major $major)
    {
        $this->majorRepository->deleteChildren($major->id);
        $this->majorRepository->delete($major->id);
        return response()->json([
            'success' => 'Xóa chuyên ngành thành công'
        ], 200);
    }

    public function getSubjects(Major $major)
    {
        $listData = $major->subjects()->simplePaginate(5);
        $listData = $listData->toArray();
        $listData['total'] = $major->subjects->count();
        return response()->json($listData,200);
    }
}
