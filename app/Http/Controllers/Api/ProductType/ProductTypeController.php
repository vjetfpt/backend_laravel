<?php

namespace App\Http\Controllers\Api\ProductType;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductType\StoreProductTypeRequest;
use App\Http\Requests\ProductType\UpdateProductTypeRequest;
use App\Interfaces\ProductType\ProductTypeInterface;
use App\Models\ProductType;
use Illuminate\Http\Request;

class ProductTypeController extends Controller
{
    protected $productTypeRepository;

    public function __construct(ProductTypeInterface $productTypeInterface)
    {
        $this->productTypeRepository = $productTypeInterface;
    }

    public function getAll()
    {
        if (request()->page != null && request()->pageLength != null) {
            $page = request()->page;
            $pagelength = request()->pageLength;
            $product_types = $this->productTypeRepository->getDataPaginate($page, $pagelength);
        } else {
            $product_types = $this->productTypeRepository->getAll();
        }
        return response()->json([
            'product_types' => $product_types,
            'total' => $this->productTypeRepository->countRecord()
        ], 200);
    }

    public function getById(ProductType $productType)
    {
        $product_type = $this->productTypeRepository->find($productType);
        return response()->json(['product_type' => $product_type], 200);
    }

    public function create(StoreProductTypeRequest $request)
    {
        $data = $request->get('product_type', []);
        $product_type = $this->productTypeRepository->create($data);
        return response()->json(['message' => 'Thêm mới thành công', 'product_type' => $product_type], 200);
    }

    public function update(ProductType $productType, UpdateProductTypeRequest $request)
    {
        $data = $request->get('product_type', []);
        $product_type = $this->productTypeRepository->update($productType->id, $data);
        return response()->json(['message' => 'Sửa thành công', 'product_type' => $product_type], 200);
    }

    public function delete(ProductType $productType)
    {
        $product_type = $this->productTypeRepository->delete($productType->id);
        return response()->json(['message' => 'Xóa thành công'], 200);
    }
}
