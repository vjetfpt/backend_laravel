<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Libraries\HelperFunction;
use App\Http\Libraries\Mail;
use App\Http\Libraries\RedisQueue;
use App\Http\Requests\StoreUserRequest;
use App\Interfaces\User\UserInterface;
use App\Http\Requests\ImportExcel\ExcelRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\UserMajor;
use Illuminate\Support\Str;
class UserController extends Controller
{
    protected $userRepository;
    protected $helperFunction;
    protected $mail;

    public function __construct(
        UserInterface $userInterface,
        Mail $mail,
        HelperFunction $helperFunction
    )
    {
        $this->userRepository = $userInterface;
        $this->mail = $mail;
        $this->helperFunction = $helperFunction;
    }

    public function getAll()
    {
        if(request()->page != null && request()->pageLength != null){
            $page = request()->page;
            $pagelength = request()->pageLength;
            $users = $this->userRepository->getDataPaginate($page, $pagelength);
        }else{
            $users = $this->userRepository->getAll();
        }
        $users = !empty($users) ? $users->load(['roles', 'majors'])
                                ->where('campus_id', Auth::user()->campus_id): [];
        $listUsers = [];
        if(!empty($users)){
            foreach($users as $user){
                $listUsers[] = $user;
            }
        }
        return response()->json([
            'users' => $listUsers,
            'total' => $this->userRepository->countRecord()
        ]);
    }

    public function getById($userId)
    {
        $user = $this->userRepository->find($userId);
        $user->load('roles');
        return response()->json(['user' => $user]);
    }

    public function create(StoreUserRequest $request)
    {
        $data_major['major_id'] = $request->input('major_id');
        $data = $request->only([
            'name', 'email', 'type'
        ]);
        $data['is_active'] = 1;
        $data['campus_id'] = Auth::user()->campus_id;
        if(isset($data['type']) && $data['type'] == 1){
            $data['teacher_is'] = 1;
        }

        if(isset($data['type']) && $data['type'] == 3){
            $data['faculty_chairman_is'] = 1;
        }
        if(isset($data['type']) && $data['type'] == 4){
            $data['teacher_is'] = 1;
        }
        $user = $this->userRepository->create($data)->load('majors');
        if(isset($data_major['major_id']) && $data_major['major_id'] != ''){
            DB::table('user_majors')->insert(
                [
                    'major_id' => $request->input('major_id'),
                    'user_id' => $user->id
                ]
            );
        }
        if(isset($data['type']) && $data['type'] == 1){
            $user->assignRole('teacher');
        }
        if(isset($data['type']) && $data['type'] == 3){
            $user->assignRole('faculty_chairman');
        }
        if(isset($data['type']) && $data['type'] == 4){
            $user->assignRole('ministry');
        }
        return response()->json($user);
    }

    public function import(ExcelRequest $request)
    {
        try {
            DB::beginTransaction();
            // $redis_queue = new RedisQueue();
            //            $del = $redis_queue->del('email');
            $file = $request->file('excel');
            $fileName = substr(
                $file->getClientOriginalName(),
                0,
                strlen($file->getClientOriginalName()) - 5
            );
            $file_path = $fileName . "-" . uniqid() . "." . $file->extension();
            $file->move(public_path('uploads/excel'), $file_path);
            Excel::import($this->userRepository, "uploads/excel/$file_path");
            return response()->json(['message' => 'Upload điểm sinh viên thành công']);
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }
    public function export()
    {
        $fileDowload = "./template_excel/template_excel.xlsx";
        return response()->download($fileDowload);
    }

    public function update($userId, Request $request)
    {
        $data = $request->get('user', []);
        $user = $this->userRepository->find($userId);
        $user_login = Auth::user();
        $list_role_user_login = $user_login->roles->pluck('name')->toArray();
        if (isset($data['role'])) {
            if (in_array('ministry', $data['role'])) {
                if (in_array('superadmin', $list_role_user_login) == false) {
                    return response()->json(['errors' => 'Bạn không có quyền cập nhập quyền'], 403);
                }
            }
            if (in_array('faculty_chairman', $data['role'])) {
                if (in_array('superadmin', $list_role_user_login) == false && in_array('ministry', $list_role_user_login) == false) {
                    return response()->json(['errors' => 'Bạn không có quyền cập nhập quyền'], 403);
                }
            }
            if (in_array('teacher', $data['role'])) {
                if (in_array('superadmin', $list_role_user_login) == false && in_array('ministry', $list_role_user_login) == false && in_array('faculty_chairman', $list_role_user_login) == false) {
                    return response()->json(['errors' => 'Bạn không có quyền cập nhập quyền'], 403);
                }
            }
            if (in_array('superadmin', $data['role'])) {
                if (in_array('superadmin', $list_role_user_login) == false || in_array('superadmin', $data['role'])) {
                    return response()->json(['errors' => 'Bạn không có quyền cập nhập quyền'], 403);
                }
            }
            $user->syncRoles($data['role']);
            $list_role_user_update = $user->roles->pluck('name')->toArray();
            if (isset($list_role_user_update)) {
                if (in_array('superadmin', $list_role_user_update)) {
                    $data_update_user['superadmin_is'] = 1;
                } else {
                    $data_update_user['superadmin_is'] = null;
                }
                if (in_array('ministry', $list_role_user_update)) {
                    $data_update_user['ministry_is'] = 1;
                } else {
                    $data_update_user['ministry_is'] = null;
                }
                if (in_array('faculty_chairman', $list_role_user_update)) {
                    $data_update_user['faculty_chairman_is'] = 1;
                    $data_update_user['type'] = config('common.user_type.master_teacher');
                } else {
                    $data_update_user['faculty_chairman_is'] = null;
                }
                if (in_array('teacher', $list_role_user_update)) {
                    $data_update_user['teacher_is'] = 1;
                } else {
                    $data_update_user['teacher_is'] = null;
                }
                if ($data_update_user) {
                    $user->update($data_update_user);
                }
            }
        }
        return response()->json([
            'success' => 'Cập nhật thành viên thành công',
            'user' => $user
        ], 200);
    }

    public function getProductTeacher(Request $request){
        return $this->userRepository->getProduct($request->user_id);
    }

    public function updateProfile(Request $request)
    {
        $user_login = Auth::user();
        $avatar = $request->file('avatar');
        $file_avatar_name = Str::uuid() . "-" . $avatar->getClientOriginalName();
        if ($avatar) {
            $this->helperFunction->deleteImageBase64($user_login->avatar);
        }
        $url = $this->helperFunction->saveImage($avatar, 'users', $file_avatar_name);
        $data_update = [
            'name' => $request->name,
            'avatar' => $url,
        ];
        $user = $this->userRepository->update($user_login->id, $data_update);
        $user->load('roles');
        return response()->json(['success' => 'Xóa thành viên thành công', 'user' => $user], 200);
    }

    public function delete(User $userId)
    {
        UserMajor::where('user_id', $userId->id)->delete();
        $this->helperFunction->deleteImageBase64($userId->avatar);
        $userId->delete($userId);
        return response()->json(['success' => 'Xóa thành viên thành công'], 200);
    }
}
