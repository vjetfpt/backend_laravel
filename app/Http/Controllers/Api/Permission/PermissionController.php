<?php

namespace App\Http\Controllers\Api\Permission;

use App\Http\Controllers\Controller;
use App\Interfaces\Permission\PermissionInterface;
use Illuminate\Http\Request;
use App\Models\Permission;
use Illuminate\Support\Facades\DB;

class PermissionController extends Controller
{

    protected $permissionRepository;

    public function __construct(PermissionInterface $permissionInterface)
    {
        $this->permissionRepository = $permissionInterface;
    }

    public function getAll()
    {
        if (request()->page != null && request()->pageLength != null) {
            $page = request()->page;
            $pagelength = request()->pageLength;
            $permissions = $this->permissionRepository->getDataPaginate($page, $pagelength);
        } else {
            $permissions = $this->permissionRepository->getAll();
        }
        $permissions = !empty($permissions) ? $permissions->load('viewPermission', 'roles'): [];

        return response()->json([
            'success' => 'Danh sách các quyền hệ thống',
            'total' => $this->permissionRepository->countRecord(),
            'data'    => $permissions
        ], 200);
    }

    public function getById(Permission $permission)
    {
        $permissions = $this->permissionRepository->find($permission->id);
        $permissions->load('viewPermission', 'roles');
        return response()->json([
            'success' => 'Quyền theo ID',
            'data'    => $permissions
        ], 200);
    }

    public function create(Request $request)
    {
        $data = $request->get('permissions', []);
        $data_permission = $data;
        if ($data_permission['view_permissions']) {
            unset($data_permission['view_permissions']);
        }
        $permission = $this->permissionRepository->create($data_permission);
        foreach ($data['view_permissions'] as $key => $value) {
            $data['view_permissions'][$key]['permission_id'] = $permission->id;
            $data['view_permissions'][$key]['created_at'] = now();
            $data['view_permissions'][$key]['updated_at'] = now();
        }
        DB::table('view_permissions')->insert($data['view_permissions']);
        $permission->load('viewPermission');
        return response()->json([
            'success' => 'Thêm quyền thành công',
            'data'    => $permission
        ], 200);
    }


    public function update(Permission $permission, Request $request)
    {
        $data = $request->get('permissions', []);
        $data_permission = $data;
        if ($data_permission['view_permissions']) {
            unset($data_permission['view_permissions']);
        }
        $permissions = $this->permissionRepository->update($permission->id, $data_permission);
        $permissions->viewPermission()->delete();
        foreach ($data['view_permissions'] as $key => $value) {
            $data['view_permissions'][$key]['permission_id'] = $permissions->id;
            $data['view_permissions'][$key]['created_at'] = now();
            $data['view_permissions'][$key]['updated_at'] = now();
        }
        DB::table('view_permissions')->where('permission_id', $permissions->id)->insert($data['view_permissions']);
        $permissions->load('viewPermission');
        return response()->json([
            'success' => 'Cập nhật quyền thành công',
            'data'    => $permissions
        ], 200);
    }

    public function delete(Permission $permission)
    {
        $permission->viewPermission()->delete();
        $permission->delete($permission->id);
        return response()->json(['success' => 'Xóa quyền thành công'], 200);
    }
}
