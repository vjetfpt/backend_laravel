<?php

namespace App\Http\Controllers\Api\Permission;

use App\Http\Controllers\Controller;
use App\Http\Libraries\HelperFunction;
use App\Interfaces\Permission\PermissionInterface;
use App\Interfaces\ViewPermission\ViewPermissionInterface;
use App\Models\ViewPermission;
use Illuminate\Http\Request;



class ViewPermissionController extends Controller
{
    protected $viewPermissionRepository;
    protected $permissionRepository;
    protected $helperFunction;

    public function __construct(
        ViewPermissionInterface $viewPermissionInterface,
        HelperFunction $helperFunction,
        PermissionInterface $permissionInterface
    )
    {
        $this->viewPermissionRepository = $viewPermissionInterface;
        $this->helperFunction = $helperFunction;
        $this->permissionRepository = $permissionInterface;
    }

    public function getAll(){
        $viewPermissions = $this->viewPermissionRepository->getAll();
        $viewPermissions->load('permission');
        return response()->json([
            'success' => 'Danh sách cách đường link truy cập',
            'total'   => count($viewPermissions),
            'data'    => $viewPermissions
        ]);
    }

    public function getById(ViewPermission $viewPermission){
        $viewPermissions = $this->viewPermissionRepository->find($viewPermission->id);
        $viewPermissions->load('permission');
        return response()->json([
            'success' => 'Link theo ID',
            'data' => $viewPermissions
        ]);
    }

    public function create(Request $request){

        $permission = $this->permissionRepository->find($request->permission_id);
        if(!$permission){
            return response()->json(['error' => 'Không tồn tại quyền'], 404);
        }
        $viewPermission = $this->viewPermissionRepository->create($request->all());
        $viewPermission->load('permission');
        return response()->json([
            'success' => 'Thêm link thành công',
            'data' => $viewPermission
        ]);
    }

    public function update(ViewPermission $viewPermission, Request $request)
    {
        $permission = $this->permissionRepository->find($request->permission_id);
        if(!$permission){
            return response()->json(['error' => 'Không tồn tại quyền'], 404);
        }
        $viewPermission = $this->viewPermissionRepository->update($viewPermission->id, $request->all());
        $viewPermission->load('permission');
        return response()->json([
            'success' => 'Cập nhật link thành công',
            'data' => $viewPermission
        ]);
    }

    public function delete(ViewPermission $viewPermission)
    {
        $viewPermission->delete($viewPermission->id);
        return response()->json(['success' => 'Xóa link thành công'], 200);
    }
}
