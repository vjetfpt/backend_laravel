<?php

namespace App\Http\Controllers\Api\Client;

use App\Http\Controllers\Controller;
use App\Interfaces\Client\ClientInterface;
use App\Interfaces\Filter\FilterInterface;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    protected $clientRepository;
    protected $filterRepository;
    public function __construct(ClientInterface $clientInterface, FilterInterface $filterInterface)
    {
        $this->clientRepository = $clientInterface;
        $this->filterRepository = $filterInterface;
    }
    public function productRateHight()
    {
        return $this->clientRepository->getProduct();
    }

    public function productMajor($id)
    {
        if (request()->page != null && request()->pageLength != null) {
            $page = request()->page;
            $pageLength = request()->pageLength;
            $listProduct = $this->clientRepository->getProductMajor($id, $page, $pageLength);
        } else {
            $listProduct = $this->clientRepository->getProductMajor($id);
        }
        return response([
            'data' => $listProduct,
            'total' => count($this->clientRepository->getProductMajor($id))
        ], 200);
    }

    public function filter(Request $request)
    {
        if ($request->has('major_id')) {
            $majorId = $request->input('major_id');
            $idCustom = '';
            $type = '';
            if ($request->has('user_id')) {
                $idCustom = $request->input('user_id');
                $type = 'user';
            } else {
                $idCustom = $request->input('campus_id');
                $type = 'campus';
            }
            
            if ($request->page != null && $request->pageLength != null) {
                $page = $request->page;
                $pageLength = $request->pageLength;
                $data = $this->clientRepository->getDataPaginate(
                    $page,
                    $pageLength,
                    $this->clientRepository->filter($idCustom, $majorId, $type)
                );
            } else {
                $data = $this->clientRepository->filter($idCustom, $majorId, $type);
            }
            return response()->json([
                'data' => $data
            ]);
        } elseif ($request->has('subject_id')) {
            $data = [];
            $subjectId = $request->input('subject_id');
            $listProduct = $this->filterRepository->filterId($subjectId, 'subject')
                ->where('status', config('common.product_status.success'));
            foreach ($listProduct as $item) {
                $data[] = $item;
            }
            return response()->json([
                'data' => $data
            ]);
        } else {
            return response()->json([
                'message' => 'Trường dữ liệu gửi lên không đủ, hoặc sai tên!'
            ]);
        }
    }

    public function search(Request $request)
    {
        if ($request->has('major_id') && $request->has('text')) {
            $products = $this->clientRepository->getProductMajor($request->input('major_id'));
            if (empty($products)) {
                return response()->json([
                    'data' => null
                ]);
            } else {
                $listProduct = null;
                $text = $request->input('text');
                foreach ($products as $item) {
                    if (
                        str_contains($item['name'], $text)
                        || str_contains(strtoupper($item['name']), strtoupper($text))
                        || str_contains(strtolower($item['name']), strtolower($text))
                    ) {
                        $listProduct[] = $item;
                    }
                }
                return response()->json([
                    'data' => $listProduct
                ]);
            }
        } else {
            return response()->json([
                'message' => 'Không đủ trường dữ liệu gửi lên hoặc trường gửi lên không đúng!'
            ]);
        }
    }

    public function sort(Request $request)
    {
        if ($request->has('majorId') && $request->has('sortBy')) {
            $majorId = $request->input('majorId');
            $sort = $request->input('sortBy');
            return response()->json([
                'data' => $this->clientRepository->sort($sort, $majorId)
            ]);
        } else {
            return response([
                'message' => 'Thiếu trường dữ liệu gửi lên'
            ]);
        }
    }
}
