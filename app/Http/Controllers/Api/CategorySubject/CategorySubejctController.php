<?php

namespace App\Http\Controllers\Api\CategorySubject;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategorySubject\CategorySubjectRequest;
use App\Http\Requests\CategorySubject\UpdateCategorySubjectRequest;
use App\Interfaces\CategorySubject\CategorySubjectInterface;
use App\Interfaces\User\UserInterface;
use App\Models\Catesubject;
use Illuminate\Http\Request;

class CategorySubejctController extends Controller
{

    protected $categorySubjectRepository;
    protected $userRepository;

    public function __construct(
        CategorySubjectInterface $categorySubjectInterface,
        UserInterface $userInterface
    )
    {
        $this->categorySubjectRepository = $categorySubjectInterface;
        $this->userRepository = $userInterface;
    }

    public function getAll()
    {
        $category_subjects = $this->categorySubjectRepository->getAll();
        $category_subjects->load('subjects', 'user');
        return response()->json([
            'success' => 'Danh sách bộ môn',
            'data' => $category_subjects
        ], 200);
    }

    public function find(Catesubject $cateSubject)
    {
        $category_subject = $this->categorySubjectRepository->find($cateSubject->id);
        $category_subject->load('subjects', 'user');
        return response()->json([
            'data' => $category_subject
        ], 200);
    }

    public function create(CategorySubjectRequest $request)
    {
        $user = $this->userRepository->find($request->user_id);
        if (!$user) {
            return response()->json([
                'error' => 'Không tìm thấy giảng viên'
            ], 404);
        }
        $category_subject = $this->categorySubjectRepository->create($request->all());
        $category_subject->load('subjects', 'user');
        return response()->json([
            'success' => 'Thêm bộ môn thành công',
            'data'    => $category_subject
        ], 200);
    }


    public function update(Catesubject $cateSubject, UpdateCategorySubjectRequest $request)
    {
        $user = $this->userRepository->find($request->user_id);
        if (!$user) {
            return response()->json([
                'error' => 'Không tìm thấy giảng viên'
            ], 404);
        }
        
        $category_subject = $this->categorySubjectRepository->update($cateSubject->id, $request->all());
        $category_subject->load('subjects', 'user');
        return response()->json([
            'success' => 'Cập nhật bộ môn thành công',
            'data'    => $category_subject
        ], 200);
    }

    public function delete(Catesubject $cateSubject)
    {
        $cateSubject->subjects()->delete();
        $this->categorySubjectRepository->delete($cateSubject->id);
        return response()->json([
            'success' => 'Xóa bộ môn thành công'
        ], 200);
    }
   
}
