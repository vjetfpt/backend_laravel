<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = $request->user();
        if (isset($user->campuses->code)) {
            return $next($request);
        }
        if ($request->expectsJson()) {
            return response()->json(['errors' => 'Tài khoản không có quyền truy cập'], 403);
        }
    }
}
