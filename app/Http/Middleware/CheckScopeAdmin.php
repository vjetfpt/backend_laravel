<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckScopeAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = $request->user();
        if ($user->superadmin_is || $user->ministry_is) {
            return $next($request);
        }
        if ($request->expectsJson()) {
            return response()->json(['errors' => 'Tài khoản không có truyền truy cập'], 403);
        }
    }
}
