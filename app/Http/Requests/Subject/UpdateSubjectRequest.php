<?php

namespace App\Http\Requests\Subject;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateSubjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' =>[
                'required',
                'max:100',
                'min:5',
                Rule::unique('subjects', 'name')->ignore($this->subject->id)
            ],
            'code' =>[
                'required',
                'max:100',
                'min:3',
                Rule::unique('subjects', 'code')->ignore($this->subject->id)
            ],
            'major_id' => 'required'
        ];
    }

    public function messages()
    {
        return[
            'name.required' => 'Nhập tên môn học',
            'name.unique' => 'Tên môn học đã tồn tại',
            'name.max' => 'Tên môn học không được dài quá',
            'name.min' => 'Tên môn học  quá ngắn',
            'code.required' => 'Nhập mã môn học',
            'code.unique' => 'Mã môn học đã tồn tại',
            'code.max' => 'Mã môn học không được dài quá',
            'code.min' => 'Mã môn học quá ngắn',
            'major_id.required' => 'Chọn chuyên ngành môn học'
        ];
    }
}
