<?php

namespace App\Http\Requests\Product;

use App\Rules\product\checkMultipleImage;
use Illuminate\Foundation\Http\FormRequest;

class StoreGalleriesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'galleries' => [
                'required',
                'array',
                new checkMultipleImage()
            ]
        ];
    }
    public function messages()
    {
        return [
            'galleries.required' => 'Bộ sưu tầm ảnh không được để trống',
            'galleries.array' => 'Bộ sưu tầm ảnh gửi lên không phải dạng mảng các ảnh'
        ];
    }
}
