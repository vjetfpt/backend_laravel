<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class StorageUploadImage extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'required|mimes:jpg,png|max:10240'
        ];
    }

    public function messages()
    {
        return [
            'image.required' => 'Chưa gủi file ảnh',
            'image.mimes' => 'Không đúng định dạng file ảnh'
        ];
    }
}
