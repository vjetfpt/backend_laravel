<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class StoreDocumentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3',
            'resource_url' => 'required|file'
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Không được để trống tên dự án',
            'name.min' => 'Tên dự án nhỏ hơn 3 ký tự',
            'resource_url.required' => 'File tài liệu không được để trống',
            'resource_url.file' => 'Tài liệu không phải định dạng file'
        ];
    }
}
