<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'class' => 'required',
            'product_type_id' => 'required',
            'video_url' => 'required',
            'description' => 'required',
            'name' => 'required',
            'resource_url' => 'required',
            'image_url' => 'required',
            'galleries' => 'required | array',
            'students' => 'required | array'
        ];
    }

    public function attributes()
    {
        return [
            'class' => 'Tên lớp',
            'product_type_id' => 'Loại sản phẩm',
            'video_url' => 'Link video',
            'description' => 'Miêu tả',
            'name' => 'Tên sản phẩm',
            'resource_url' => 'Link tài liệu',
            'image_url' => 'Link ảnh',
            'galleries' => 'Bộ sưu tầm',
            'students' => 'Danh sách sinh viên'
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute không được để trống',
            'galleries.array' => 'Bộ sưu tầm không đúng dạng mảng gửi lên',
            'students.array' => 'Danh sách sinh viên không đúng dạng mảng gửi lên',
        ];
    }
}
