<?php

namespace App\Http\Requests\Product;

use App\Rules\product\checkCampusIdInDb;
use App\Rules\product\checkProductTypeIdInDb;
use App\Rules\product\checkSubjectIdInDb;
use App\Rules\product\checkTeacherIdInDB;
use App\Rules\transcript\checkSemesterInDB;
use Illuminate\Foundation\Http\FormRequest;
class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:100',
            'product_type_id' => [
                'required',
                new checkProductTypeIdInDb()
            ],
            'class' => 'required',
            'image_url' => 'required',
            'description' => 'required',
            'video_url' => 'required|min:5',
            'resource_url' => 'required',
            'galleries' => 'required|array',
            'students' => [
                'required',
                'array'
            ]
        ];

    }
    public function messages()
    {
        return [
            'required' => ':attribute không được để trống',
            'name.min' => 'Tên dự án quá ngắn',
            'name.max' => 'Tên dự án vượt quá ký tự cho phép',
            'video_url.min' => 'Tên đường dẫn video quá ngắn',
            'galleries.array' => 'Sai định dạng gửi lên ',
            'students.array' => 'Không đúng định dạng mảng email của sinh viên' 
        ];
    }
    public function attributes()
    {
        return [
            'name'=> 'Tên dự án',
            'product_type_id' => 'Kỳ học',
            'class' => 'Lớp học',
            'image_url' => 'Link ảnh đại diện',
            'description' => 'Miêu tả',
            'video_url' => 'Link video',
            'resource_url' => 'Link tài liệu',
            'students' => 'Danh sách email sinh viên'
        ];
    }
}
