<?php

namespace App\Http\Requests\Semester;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\semester\checkNameUpdate;
class StoreSemesterRequestUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'min:1',
                new checkNameUpdate()
            ]
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Tên không được để trống',
            'name.min' => 'Ký tự của tên kỳ nhỏ hơn 1',
            'name.unique' => 'Tên kỳ đã tồn tại',
        ];
    }
}
