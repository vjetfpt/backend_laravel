<?php

namespace App\Http\Requests\CategorySubject;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CategorySubjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' =>[
                'required',
                'max:100',
                'min:5',
                Rule::unique('catesubjects')->ignore($this->id),
            ],
            'code' =>[
                'required',
                'max:100',
                'min:3',
                Rule::unique('catesubjects')->ignore($this->id),
            ]
        ];
    }

    public function messages()
    {
        return[
            'name.required' => 'Nhập bộ môn học',
            'name.unique' => 'Tên bộ môn học đã tồn tại',
            'name.max' => 'Tên bộ môn học không được dài quá',
            'name.min' => 'Tên bộ môn học  quá ngắn',
            'code.required' => 'Nhập mã bộ môn học',
            'code.unique' => 'Mã bộ môn học đã tồn tại',
            'code.max' => 'Mã bộ môn học không được dài quá',
            'code.min' => 'Mã bộ môn học quá ngắn',
        ];
    }

}
