<?php

namespace App\Http\Requests\ProductType;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateProductTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_type' => 'required|array',
            'product_type.name' => [
                "required",
                Rule::unique('product_types' , 'name')->ignore($this->productType->id)
            ]
        ];
    }

    public function messages()
    {
       return [
           'product_type.required' => 'Không để trống product_type',
           'product_type.name.required' => 'Không để trống tên product_type',
       ];
    }
}
