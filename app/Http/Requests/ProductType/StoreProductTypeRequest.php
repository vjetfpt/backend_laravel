<?php

namespace App\Http\Requests\ProductType;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_type' => 'required|array',
            'product_type.name' => 'required|unique:product_types,name',
        ];
    }

    public function messages()
    {
        return [
            'product_type.required' => 'Không để trống product type',
            'product_type.array' => 'product_type phải là 1 array',
            'product_type.name.required' => 'Không để trống tên loại sản phẩm',
            'product_type.name.unique' => 'Loại sản phẩm đã tồn tại'
        ];
    }
}
