<?php

namespace App\Http\Requests\ImportExcel;
use App\Rules\product\checkCampusIdInDb;
use Illuminate\Foundation\Http\FormRequest;
use App\Rules\transcript\checkSemesterInDB;
class ExcelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'excel'=>[
                'required',
                'mimes:xlsx'
            ],
            'campus_id' => [
                'required',
                new checkCampusIdInDb()
            ],
            'semester_id' => [
                'required',
                new checkSemesterInDB()
            ]
        ];
    }
    public function messages()
    {
        return [
            'excel.mimes' => 'Bạn cần chọn đúng file excel có định dạng đuôi .xlsx',
            'excel.required' => 'Chưa nhập file excel',
            'campus_id.required' => 'Chưa chọn cơ sở',
            'semester_id.required' => 'Chưa chọn kỳ học' 
        ];
    }
}
