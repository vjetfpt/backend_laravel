<?php

namespace App\Http\Requests\Major;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateMajorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' =>[
                'required',
                'max:100',
                'min:3',
                Rule::unique('majors', 'name')->ignore($this->major),
            ],
        ];
    }

    public function messages()
    {
        return[
            'name.required' => 'Nhập tên chuyên ngành',
            'name.unique' => 'Tên chuyên ngành đã tồn tại',
            'name.max' => 'Tên chuyên ngành không được dài quá',
            'name.min' => 'Tên chuyên ngành  quá ngắn',
        ];
    }
}
