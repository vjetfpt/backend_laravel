<?php

namespace App\Rules\Excel;

use App\Models\Subject;
use Illuminate\Contracts\Validation\Rule;

class checkSubjectCode implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    private $maMon;
    public function __construct($maMon)
    {
        $this->maMon = $maMon;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $subject = Subject::where('code',$value)->exists();
        if(!$subject){
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "Mã môn $this->maMon không tồn tại.";
    }
}
