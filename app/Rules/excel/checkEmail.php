<?php

namespace App\Rules\Excel;

use Illuminate\Contracts\Validation\Rule;

class checkEmail implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    private $email;
    public function __construct($email)
    {
        $this->email = $email;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!strpos($value, '@fpt.edu.vn')) {
            return false;
        }
        if (!filter_var($value, FILTER_SANITIZE_NUMBER_INT)) {
            return false;
        };
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "$this->email không thuộc mail của fpoly.";
    }
}
