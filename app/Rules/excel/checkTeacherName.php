<?php

namespace App\Rules\Excel;

use App\Models\Subject;
use App\Models\User;
use App\Models\UserMajor;
use Illuminate\Contracts\Validation\Rule;

class checkTeacherName implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    private $nameTeacher;
    private $subjectCode;
    private $message = '';
    public function __construct($nameTeacher, $subjectCode)
    {
        $this->nameTeacher = $nameTeacher;
        $this->subjectCode = $subjectCode;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $teacher = User::where('name', $value)
            ->whereIn(
                'type',
                [config('common.user_type.teacher'), config('common.user_type.master_teacher')]
            )
            ->exists();
        if (!$teacher) {
            $this->message .= "Tên giảng viên $this->nameTeacher không tồn tại,";
            return false;
        }
        $teacher = User::where('name',$this->nameTeacher)
        ->whereIn('type',
            [config('common.user_type.teacher'),config('common.user_type.master_teacher')]);
        $subject = Subject::where('code',$this->subjectCode);
        if($teacher->exists() && $subject->exists()){
            $majorId = $subject->first()->major_id;
            $listMajorId = UserMajor::where('user_id',$teacher->first()->id)->pluck('major_id')
                                    ->toArray();
            if(!in_array($majorId,$listMajorId)){
                $this->message .= "$this->nameTeacher không quản lý môn có mã $this->subjectCode";
            }
        }
        if(!empty($this->message)){
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
