<?php

namespace App\Rules\product;

use App\Models\User;
use Illuminate\Contracts\Validation\Rule;

class checkTeacherIdInDB implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user = User::find($value);
        if(!$user || $user->type == config('common.user_type.student')){
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Mã giảng viên không tồn tại';
    }
}
