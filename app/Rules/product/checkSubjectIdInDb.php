<?php

namespace App\Rules\product;

use App\Models\Subject;
use Illuminate\Contracts\Validation\Rule;

class checkSubjectIdInDb implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $subject = Subject::find($value);
        if(!$subject){
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Môn học không tồn tại.';
    }
}
