<?php

namespace App\Rules\product;

use Illuminate\Contracts\Validation\Rule;

class checkMultipleImage implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $listImage = $value;
        $arr_allow = ['jpg','png','svg','gif'];
        for($i=0;$i<count($listImage);$i++){
            if(!in_array($listImage[$i]->extension(),$arr_allow)){
                return false;
            }
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Một vài ảnh trong bộ sưu tầm không phải định dạng ảnh';
    }
}
