<?php

namespace App\Rules\Product;

use Illuminate\Contracts\Validation\Rule;

class checkListEmailStudent implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $listEmail = $value;
        foreach ($listEmail as $email) {
            if (!strpos($email, '@fpt.edu.vn')) {
                return false;
            }
            if (!filter_var($email, FILTER_SANITIZE_NUMBER_INT)) {
                return false;
            };
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Danh sách email gửi lên không đúng định dạng email sinh viên fpt.';
    }
}
