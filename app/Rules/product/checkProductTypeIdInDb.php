<?php

namespace App\Rules\product;

use App\Models\ProductType;
use Illuminate\Contracts\Validation\Rule;

class checkProductTypeIdInDb implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $productType = ProductType::find($value);
        if(!$productType){
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Không tồn tại kiểu loại sản phẩm.';
    }
}
