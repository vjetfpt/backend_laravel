<?php

namespace App\Rules\transcript;

use App\Models\Semester;
use Illuminate\Contracts\Validation\Rule;

class checkSemesterInDB implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $semester = Semester::find($value);
        if(!$semester){
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Kỳ học không tồn tại.';
    }
}
