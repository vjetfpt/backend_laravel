<?php
return [
    'send_mail' => [
        'status' => [
            'wait_send' => 0,
            'sent' => 1,
            'clicked' => 2
        ]
    ],
    'user_type' => [
        'student' => 2,
        'teacher' => 1,
        'master_teacher' => 3,
        'admin' => 4
    ],
    'product_status' => [
        'wait_update' => 0,
        'wait_confirm1' => 1,
        'wait_confirm2' => 2,
        'success' => 3
    ]
];
