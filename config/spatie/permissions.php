<?php 

return [
    [
        "id" => 1,
        "name" => "view_product",
        "guard_name" => config('auth.defaults.guard'),
        "type" => 0,
        "status" => 1
    ],
    [
        "id" => 2,
        "name" => "add_product",
        "guard_name" => config('auth.defaults.guard'),
        "type" => 0,
        "status" => 1
    ],
    [
        "id" => 3,
        "name" => "update_product",
        "guard_name" => config('auth.defaults.guard'),
        "type" => 0,
        "status" => 1
    ],
    [
        "id" => 4,
        "name" => "delete_product",
        "guard_name" => config('auth.defaults.guard'),
        "type" => 0,
        "status" => 1
    ],
    [
        "id" => 5,
        "name" => "view_role",
        "guard_name" => config('auth.defaults.guard'),
        "type" => 0,
        "status" => 2
    ],
    [
        "id" => 6,
        "name" => "add_role",
        "guard_name" => config('auth.defaults.guard'),
        "type" => 0,
        "status" => 2
    ],
    [
        "id" => 7,
        "name" => "update_role",
        "guard_name" => config('auth.defaults.guard'),
        "type" => 0,
        "status" => 2
    ],
    [
        "id" => 8,
        "name" => "delete_role",
        "guard_name" => config('auth.defaults.guard'),
        "type" => 0,
        "status" => 2
    ],
    [
        "id" => 9,
        "name" => "view_permission",
        "guard_name" => config('auth.defaults.guard'),
        "type" => 0,
        "status" => 2
    ],
    [
        "id" => 10,
        "name" => "add_permission",
        "guard_name" => config('auth.defaults.guard'),
        "type" => 0,
        "status" => 2
    ],
    [
        "id" => 11,
        "name" => "update_permission",
        "guard_name" => config('auth.defaults.guard'),
        "type" => 0,
        "status" => 2
    ],
    [
        "id" => 12,
        "name" => "delete_permission",
        "guard_name" => config('auth.defaults.guard'),
        "type" => 0,
        "status" => 2
    ],
    [
        "id" => 13,
        "name" => "view_product_type",
        "guard_name" => config('auth.defaults.guard'),
        "type" => 0,
        "status" => 1
    ],
    [
        "id" => 14,
        "name" => "add_product_type",
        "guard_name" => config('auth.defaults.guard'),
        "type" => 0,
        "status" => 1
    ],
    [
        "id" => 15,
        "name" => "update_product_type",
        "guard_name" => config('auth.defaults.guard'),
        "type" => 0,
        "status" => 1
    ],
    [
        "id" => 16,
        "name" => "delete_product_type",
        "guard_name" => config('auth.defaults.guard'),
        "type" => 0,
        "status" => 1
    ],
    [
        "id" => 17,
        "name" => "view_major",
        "guard_name" => config('auth.defaults.guard'),
        "type" => 0,
        "status" => 1
    ],
    [
        "id" => 18,
        "name" => "add_major",
        "guard_name" => config('auth.defaults.guard'),
        "type" => 0,
        "status" => 1
    ],
    [
        "id" => 19,
        "name" => "update_major",
        "guard_name" => config('auth.defaults.guard'),
        "type" => 0,
        "status" => 1
    ],
    [
        "id" => 20,
        "name" => "delete_major",
        "guard_name" => config('auth.defaults.guard'),
        "type" => 0,
        "status" => 1
    ],
    [
        "id" => 21,
        "name" => "view_subject",
        "guard_name" => config('auth.defaults.guard'),
        "type" => 0,
        "status" => 1
    ],
    [
        "id" => 22,
        "name" => "add_subject",
        "guard_name" => config('auth.defaults.guard'),
        "type" => 0,
        "status" => 1
    ],
    [
        "id" => 23,
        "name" => "update_subject",
        "guard_name" => config('auth.defaults.guard'),
        "type" => 0,
        "status" => 1
    ],
    [
        "id" => 24,
        "name" => "delete_subject",
        "guard_name" => config('auth.defaults.guard'),
        "type" => 0,
        "status" => 1
    ],
    [
        "id" => 25,
        "name" => "view_semester",
        "guard_name" => config('auth.defaults.guard'),
        "type" => 0,
        "status" => 1
    ],
    [
        "id" => 26,
        "name" => "add_semester",
        "guard_name" => config('auth.defaults.guard'),
        "type" => 0,
        "status" => 1
    ],
    [
        "id" => 27,
        "name" => "update_semester",
        "guard_name" => config('auth.defaults.guard'),
        "type" => 0,
        "status" => 1
    ],
    [
        "id" => 28,
        "name" => "delete_semester",
        "guard_name" => config('auth.defaults.guard'),
        "type" => 0,
        "status" => 1
    ],
    [
        "id" => 29,
        "name" => "view_user",
        "guard_name" => config('auth.defaults.guard'),
        "type" => 0,
        "status" => 2
    ],
    [
        "id" => 30,
        "name" => "add_user",
        "guard_name" => config('auth.defaults.guard'),
        "type" => 0,
        "status" => 2
    ],
    [
        "id" => 31,
        "name" => "update_user",
        "guard_name" => config('auth.defaults.guard'),
        "type" => 0,
        "status" => 2
    ],
    [
        "id" => 32,
        "name" => "delete_user",
        "guard_name" => config('auth.defaults.guard'),
        "type" => 0,
        "status" => 2
    ],
    [
        "id" => 33,
        "name" => "import",
        "guard_name" => config('auth.defaults.guard'),
        "type" => 0,
        "status" => 3
    ],
    [
        "id" => 34,
        "name" => "approve_teacher",
        "guard_name" => config('auth.defaults.guard'),
        "type" => 0,
        "status" => 1
    ],
    [
        "id" => 35,
        "name" => "settings",
        "guard_name" => config('auth.defaults.guard'),
        "type" => 0,
        "status" => 4
    ],
    [
        "id" => 36,
        "name" => "chairman_approved",
        "guard_name" => config('auth.defaults.guard'),
        "type" => 0,
        "status" => 1
    ],
    [
        "id" => 37,
        "name" => "view_feedback",
        "guard_name" => config('auth.defaults.guard'),
        "type" => 0,
        "status" => 1
    ],
    [
        "id" => 38,
        "name" => "view_comment",
        "guard_name" => config('auth.defaults.guard'),
        "type" => 0,
        "status" => 1
    ]
];
?>