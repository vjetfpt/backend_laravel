<?php 

return [
    [
        "id" => 1,
        "title" => "Quản trị kì học",
        "url" => "/semester",
        "icon" => "<BiSitemap />",
        "permission_id" => 25,
        "status" => 1
    ],
    [
        "id" => 2,
        "title" => "Quản trị sản phẩm",
        "url" => "/confirm",
        "icon" => "<BsBag />",
        "permission_id" => 1,
        "status" => 1
    ],
    [
        "id" => 3,
        "title" => "Quản trị danh mục",
        "url" => "/product-type",
        "icon" => "<FiType />",
        "permission_id" => 13,
        "status" => 1
    ],
    [
        "id" => 4,
        "title" => "Quản trị chuyên ngành",
        "url" => "/majors",
        "icon" => "<FiBookOpen />",
        "permission_id" => 17,
        "status" => 1
    ],
    [
        "id" => 5,
        "title" => "Quản trị môn học",
        "url" => "/subjects",
        "icon" => "<BiBookAlt />",
        "permission_id" => 21,
        "status" => 1
    ],
    [
        "id" => 6,
        "title" => "Quản trị bình luận",
        "url" => "/comment",
        "icon" => "<BsChat />",
        "permission_id" => 38,
        "status" => 1
    ],
    [
        "id" => 7,
        "title" => "Quản trị phản hồi",
        "url" => "/feedback",
        "icon" => "<MdMailOutline />",
        "permission_id" => 37,
        "status" => 1
    ],
    [
        "id" => 8,
        "title" => "Quản trị vai trò",
        "url" => "/role",
        "icon" => "<BsPersonPlus />",
        "permission_id" => 5,
        "status" => 2
    ],
    [
        "id" => 9,
        "title" => "Quản trị quyền",
        "url" => "/permissions",
        "icon" => "<GiMagnifyingGlass />",
        "permission_id" => 9,
        "status" => 2
    ],
    [
        "id" => 10,
        "title" => "Quản thành viên",
        "url" => "/user",
        "icon" => "<FiUsers />",
        "permission_id" => 29,
        "status" => 3
    ],
    [
        "id" => 11,
        "title" => "Nhập điểm",
        "url" => "/upload-excel",
        "icon" => "<AiOutlineCloudUpload />",
        "permission_id" => 33,
        "status" => 4
    ],
    [
        "id" => 12,
        "title" => "Cài đặt",
        "url" => "/setting",
        "icon" => "<RiSettings4Line />",
        "permission_id" => 35,
        "status" => 5
    ],
    [
        "id" => 13,
        "title" => "Quản thành viên",
        "url" => "/user/:id",
        "icon" => "<FiUsers />",
        "permission_id" => 29,
        "status" => 3
    ]
];
