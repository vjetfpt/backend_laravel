<?php 

return [
    [
        "id" => 1,
        "name" => "superadmin",
        "title" => "Quản trị tối cao",
        "guard_name" => config('auth.defaults.guard'),
    ],
    [
        "id" => 2,
        "name" => "ministry",
        "title" => "Giáo vụ",
        "guard_name" => config('auth.defaults.guard'),
    ],
    [
        "id" => 3,
        "name" => "faculty_chairman",
        "title" => "Chủ nhiệm bộ môn",
        "guard_name" => config('auth.defaults.guard'),
    ],
    [
        "id" => 4,
        "name" => "teacher",
        "title" => "Giảng viên",
        "guard_name" => config('auth.defaults.guard'),
    ]
];



