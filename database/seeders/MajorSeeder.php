<?php

namespace Database\Seeders;

use App\Models\Major;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MajorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('majors')->truncate();
        $dataMajor = [
            [
                'name' => 'Công nghệ thông tin',
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i"),
            ],
            [
                'name' => 'Kinh tế - Kinh doanh',
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i"),
            ],
            [
                'name' => 'Thiết kế đồ họa',
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i"),
            ],
            [
                'name' => 'Cơ khí, (điện) tự động hóa',
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i"),
            ],
            [
                'name' => 'Làm đẹp',
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i"),
            ],
            [
                'name' => 'Du lịch - nhà hàng - khách sạn',
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i"),
            ]
        ];
        DB::table('majors')->insert($dataMajor);
    }
}
