<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataProduct = [
            [
                'name' => 'Website bán điện thoại',
                'teacher_id' => 1,
                'subject_id' => 1,
                'product_type_id' => 1,
                'semester_id' => 2,
                'campus_id' => 1,
                'status' => 0,
                'image' => 'https://picsum.photos/640/480',
                'class' => 'PT15312',
                'video_url' => 'https://www.youtube.com/watch?v=NIJHqNWMtAw&list=RD6K4FY0CSga0&index=2',
                'resource_url' => 'https://gitlab.com/vjetfpt/backend_laravel',
                'description' => 'Sản phẩm này rất ok nhé',
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i")
            ],
            [
                'name' => 'Website bán đồ nội thất',
                'teacher_id' => 1,
                'subject_id' => 1,
                'product_type_id' => 1,
                'semester_id' => 2,
                'campus_id' => 1,
                'status' => 0,
                'image' => 'https://picsum.photos/640/480',
                'class' => 'PT15313',
                'video_url' => 'https://www.youtube.com/watch?v=NIJHqNWMtAw&list=RD6K4FY0CSga0&index=2',
                'resource_url' => 'https://gitlab.com/vjetfpt/backend_laravel',
                'description' => 'Sản phẩm này do sinh viên tự mô tả',
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i")
            ],
            [
                'name' => 'Phần mềm quản lí quán cà phê',
                'teacher_id' => 5,
                'subject_id' => 6,
                'product_type_id' => 3,
                'semester_id' => 2,
                'campus_id' => 1,
                'status' => 0,
                'class' => 'PT15316',
                'image' => 'https://picsum.photos/640/480',
                'video_url' => 'https://www.youtube.com/watch?v=NIJHqNWMtAw&list=RD6K4FY0CSga0&index=2',
                'resource_url' => 'https://gitlab.com/vjetfpt/backend_laravel',
                'description' => 'Phần mềm do sinh viên tự viết mô tả',
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i")
            ],
        ];
        DB::table('products')->insert($dataProduct);
    }
}
