<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();
        $data_roles = config('spatie.roles');
        foreach($data_roles as $key => $val){
            $data_roles[$key]['created_at'] = date('Y-m-d H:i:s');
            $data_roles[$key]['updated_at'] = date('Y-m-d H:i:s');
        }
        DB::table('roles')->insert($data_roles);
        
        DB::table('role_has_permissions')->truncate();
        $data_role_has_permissions = config('spatie.role_has_permissions');
        DB::table('role_has_permissions')->insert($data_role_has_permissions);
    }
}
