<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->delete();
        $data_permissions = config('spatie.permissions');
        foreach($data_permissions as $key => $val){
            $data_permissions[$key]['created_at'] = date('Y-m-d H:i:s');
            $data_permissions[$key]['updated_at'] = date('Y-m-d H:i:s');
        }
        DB::table('permissions')->insert($data_permissions);

        DB::table('view_permissions')->truncate();
        $data_view_permissions = config('spatie.view_permissions');
        foreach($data_view_permissions as $key => $val){
            $data_view_permissions[$key]['created_at'] = date('Y-m-d H:i:s');
            $data_view_permissions[$key]['updated_at'] = date('Y-m-d H:i:s');
        }
        DB::table('view_permissions')->insert($data_view_permissions);
    }
}
