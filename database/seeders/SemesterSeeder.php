<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;


class SemesterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('semesters')->truncate();
        $data = [
            [
                'name' => "Fall 2020",
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i"),
            ],
            [
                'name' => "Summer 2021",
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i"),
            ],
            [
                'name' => "Fall 2021",
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i"),
            ],
            [
                'name' => "Spring 2021",
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i"),
            ],
        ];
        DB::table('semesters')->insert($data);
    }
}
