<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        $data_member_supremacy = config('spatie.member_supremacy');
        $data_model_has_roles = config('spatie.model_has_roles');
        foreach($data_member_supremacy as $key => $val){
            $data_member_supremacy[$key]['created_at'] = date('Y-m-d h:i');
            $data_member_supremacy[$key]['updated_at'] = date('Y-m-d h:i');
        }
        DB::table('users')->insert($data_member_supremacy);
        
        DB::table('model_has_roles')->truncate();
        DB::table('model_has_roles')->insert($data_model_has_roles);
    }
}
