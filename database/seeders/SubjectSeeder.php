<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subjects')->truncate();
        $dataSubject = [
            [
                'name' => 'Tin học cơ sở',
                'code' => 'COM1012',
                'major_id' => 1,
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i"),
            ],
            [
                'name' => 'Tin học cơ sở',
                'code' => 'COM1014',
                'major_id' => 1,
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i"),
            ],
            [
                'name' => 'Tin học văn phòng',
                'code' => 'COM1024',
                'major_id' => 1,
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i"),
            ],
            [
                'name' => 'Nhập môn lập trình',
                'code' => 'COM108',
                'major_id' => 1,
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i"),
            ],
            [
                'name' => 'Cơ sở dữ liệu',
                'code' => 'COM2012',
                'major_id' => 1,
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i"),
            ],
            [
                'name' => 'Lập trình Java 1',
                'code' => 'MOB1023',
                'major_id' => 1,
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i"),
            ],
            [
                'name' => 'Nguyên lí kế toán',
                'code' => 'ACC1013',
                'major_id' => 2,
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i"),
            ],
            [
                'name' => 'Kế toán báo cáo tài chính',
                'code' => 'ACC1023',
                'major_id' => 2,
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i"),
            ],
            [
                'name' => 'Nhập môn quản trị doanh nghiệp',
                'code' => 'BUS1013',
                'major_id' => 2,
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i"),
            ],
            [
                'name' => 'Hành vi tổ chức',
                'code' => 'BUS1034',
                'major_id' => 2,
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i"),
            ],
            [
                'name' => 'Màu sắc',
                'code' => 'MUL2142',
                'major_id' => 3,
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i"),
            ],
            [
                'name' => 'Thiết kế hình ảnh với photoshop',
                'code' => 'MUL1013',
                'major_id' => 3,
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i"),
            ],
            [
                'name' => 'Thiết kế bao bì',
                'code' => 'MUL2123',
                'major_id' => 3,
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i"),
            ],
            [
                'name' => 'Nghệ thuật chữ',
                'code' => 'MUL2132',
                'major_id' => 3,
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i"),
            ],
            [
                'name' => 'Mạch điện & An toàn điênh',
                'code' => 'AUT102',
                'major_id' => 4,
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i"),
            ],
            [
                'name' => 'Điện tử cơ bản',
                'code' => 'AUT103',
                'major_id' => 4,
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i"),
            ],
            [
                'name' => 'Thiết kế mạch điện tử',
                'code' => 'AUT104',
                'major_id' => 4,
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i"),
            ],
            [
                'name' => 'Dịch vụ cắt tóc gội đầu :)',
                'code' => 'MS101',
                'major_id' => 5,
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i"),
            ],
            [
                'name' => 'Kế toán nhà hàng khách sạn',
                'code' => 'ACC105',
                'major_id' => 6,
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i"),
            ],
            [
                'name' => 'Cơ sở văn hóa',
                'code' => 'HIS101',
                'major_id' => 6,
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i"),
            ],
            [
                'name' => 'Nghiệp vụ lễ tân',
                'code' => 'HOS1011',
                'major_id' => 6,
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i"),
            ],
        ];
        DB::table('subjects')->insert($dataSubject);
    }
}
