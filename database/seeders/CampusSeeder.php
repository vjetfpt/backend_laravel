<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CampusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('campuses')->truncate();
        $dataCampus = [
            [
                'name' => "FPT Polytechnic Hà Nội",
                "code" => "FPT-HN",
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i"),
            ],
            [
                'name' => "FPT Polytechnic Đà Nẵng",
                "code" => "FPT-DN",
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i"),
            ],
            [
                'name' => "FPT Polytechnic Cần Thơ",
                "code" => "FPT-CT",
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i"),
            ],
            [
                'name' => "FPT Polytechnic Hồ Chí Minh",
                "code" => "FPT-HCM",
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i"),
            ],
            [
                'name' => "FPT Polytechnic Tây Nguyên",
                "code" => "FPT-TN",
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i"),
            ],
        ];
        DB::table('campuses')->insert($dataCampus);
    }
}
