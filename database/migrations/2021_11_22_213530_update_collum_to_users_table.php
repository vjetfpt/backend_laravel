<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCollumToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('superadmin_is')->nullable();
            $table->integer('ministry_is')->nullable();
            $table->integer('faculty_chairman_is')->nullable();
            $table->integer('teacher_is')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('superadmin_is');
            $table->dropColumn('ministry_is');
            $table->dropColumn('faculty_chairman_is');
            $table->dropColumn('teacher_is');
        });
    }
}
