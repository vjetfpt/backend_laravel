<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('token');
            $table->string('name')->nullable()->change();
            $table->string('product_type_id')->nullable()->change();
            $table->string('image')->nullable()->change();
            $table->string('description')->nullable()->change();
            $table->integer('user_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('token');
            $table->string('name')->change();
            $table->string('product_type_id')->change();
            $table->string('image')->change();
            $table->string('description')->change();
            $table->dropColumn('user_id');
        });
    }
}
