<?php

use App\Http\Controllers\Api\Role\RoleController;
use Illuminate\Support\Facades\Route;

Route::prefix('roles')->middleware('auth:sanctum')->group(function () {
    Route::get('/', [RoleController::class, 'getAll']);
    Route::get('/{role}', [RoleController::class, 'getById']);
    Route::post('/', [RoleController::class, 'create'])->middleware('permission:add_role');
    Route::put('/{role}', [RoleController::class, 'update'])->middleware('permission:update_role');
});
