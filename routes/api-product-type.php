<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\ProductType\ProductTypeController;

Route::prefix('product_types')->group(function () {
    Route::get('/', [ProductTypeController::class, 'getAll']);
    Route::get('/{productType}', [ProductTypeController::class, 'getById']);
    Route::post('/', [ProductTypeController::class, 'create']);
    Route::put('/{productType}', [ProductTypeController::class, 'update']);
    Route::delete('/{productType}', [ProductTypeController::class, 'delete']);
});
