<?php

use App\Http\Controllers\Api\Auth\AuthController;
use App\Http\Controllers\Api\Permission\PermissionController;
use App\Http\Controllers\Api\Permission\ViewPermissionController;
use Illuminate\Support\Facades\Route;

Route::prefix('permissions')->middleware('auth:sanctum')->group(function () {
    Route::get('/', [PermissionController::class, 'getAll']);
    Route::get('/{permission}', [PermissionController::class, 'getById']);
    Route::post('/', [PermissionController::class, 'create'])->middleware('permission:add_permission');
    Route::put('/{permission}', [PermissionController::class, 'update'])->middleware('permission:update_permission');
    Route::delete('/{permission}', [PermissionController::class, 'delete'])->middleware('permission:delete_permission');
});

Route::prefix('view_permissions')->middleware('auth:sanctum')->group(function () {
    Route::get('/', [ViewPermissionController::class, 'getAll']);
    Route::get('/{viewPermission}', [ViewPermissionController::class, 'getById']);
    Route::post('/', [ViewPermissionController::class, 'create']);
    Route::put('/{viewPermission}', [ViewPermissionController::class, 'update']);
    Route::delete('/{viewPermission}', [ViewPermissionController::class, 'delete']);
});