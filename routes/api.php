<?php

use App\Http\Controllers\Api\Auth\AuthController;
use App\Http\Controllers\Api\Campus\CampusController;
use App\Http\Controllers\Api\CategorySubject\CategorySubejctController;
use App\Http\Controllers\Api\Client\ClientController;
use App\Http\Controllers\Api\Major\MajorController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DocumentApi\DocumentApiController;
use App\Http\Controllers\Api\User\UserController;

use App\Http\Controllers\Api\Semester\SemesterController;
use App\Http\Controllers\Api\Product\ProductController;
use App\Http\Controllers\Api\Subject\SubjectController;
use Illuminate\Support\Facades\Redis;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::get('/', [DocumentApiController::class , 'index']);
Route::get('/wiki', [DocumentApiController::class , 'wiki']);

Route::get('/users', [UserController::class, 'getAll'])->middleware('auth:sanctum', 'permission:view_user');
Route::get('/users/{userId}', [UserController::class, 'getById']);
Route::post('/users/create' , [UserController::class , 'create'])->middleware('auth:sanctum', 'permission:add_user');
Route::put('/users/{userId}' , [UserController::class , 'update'])->middleware('auth:sanctum', 'permission:update_user');
Route::post('/users/product',[UserController::class , 'getProductTeacher'])->middleware('auth:sanctum');
Route::delete('/users/{userId}', [UserController::class, 'delete'])->middleware('auth:sanctum', 'permission:delete_user');
Route::post('/profiles' , [UserController::class , 'updateProfile'])->middleware('auth:sanctum');

Route::apiResource('/semesters',SemesterController::class)->middleware('auth:sanctum');
Route::get('/semester/paginate',[SemesterController::class,'paginate'])->middleware('auth:sanctum');

Route::prefix('majors')->middleware('auth:sanctum')->group(function () {
    Route::get('/',[MajorController::class, 'getAll']);
    Route::get('/{major}',[MajorController::class, 'find']);
    Route::post('/create',[MajorController::class, 'create'])->middleware('permission:add_major');
    Route::put('/update/{major}',[MajorController::class, 'update'])->middleware('permission:update_major');
    Route::delete('/delete/{major}',[MajorController::class, 'delete'])->middleware('permission:delete_major');
});
Route::get('majors/{major}/subjects', [MajorController::class, 'getSubjects']);

Route::get('subjects/',[SubjectController::class, 'getAll']);
Route::get('subjects/{subject}',[SubjectController::class, 'find']);
Route::prefix('subjects')->middleware('auth:sanctum')->group(function () {
    Route::post('/create',[SubjectController::class, 'create'])->middleware('permission:add_subject');
    Route::put('/update/{subject}',[SubjectController::class, 'update'])->middleware('permission:update_subject');
    Route::delete('/delete/{subject}',[SubjectController::class, 'delete'])->middleware('permission:delete_subject');
});

Route::prefix('campuses')->group(function () {
    Route::get('/',[CampusController::class, 'index']);
});
Route::post('/import',[UserController::class,'import'])->middleware('auth:sanctum', 'permission:import');

Route::post('logout', [AuthController::class, 'logout'])->middleware('auth:sanctum');
Route::get('refresh', [AuthController::class, 'refresh'])->middleware('auth:sanctum');
Route::post('callback/google', [AuthController::class, 'loginGoogleCallBack']);
Route::post('user/google', [AuthController::class, 'loginUser']);
Route::get('/export',[UserController::class,'export']);