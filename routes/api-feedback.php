<?php

use App\Http\Controllers\Api\FeedBack\FeedBackController;
use Illuminate\Support\Facades\Route;

Route::prefix('feedbacks')->group(function () {
    Route::get('/', [FeedBackController::class, 'getAll']);
    Route::post('/', [FeedBackController::class, 'create']);
    Route::get('/{feedbacks}', [FeedBackController::class, 'find']);
    Route::delete('/{feedbacks}', [FeedBackController::class, 'delete']);
});
