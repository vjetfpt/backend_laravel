<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Product\ProductController;

Route::prefix('products')->group(function () {
    Route::get('/', [ProductController::class, 'getAll']);
    Route::get('/{product}', [ProductController::class, 'getById']);
    Route::post('/', [ProductController::class, 'updateFirst'])->middleware('auth:sanctum');;
    Route::post('/galleries' , [ProductController::class , 'uploadMultipleImage']);
    Route::post('/document', [ProductController::class, 'uploadDocument']);
    Route::post('/image', [ProductController::class, 'uploadImage']);
    Route::post('/getinfo',[ProductController::class, 'getInfo']);
    Route::put('/{product}',[ProductController::class, 'update']);
    Route::post('/video',[ProductController::class, 'uploadVideo']);
    Route::delete('/{product}',[ProductController::class, 'delete']);
    Route::post('/createimage',[ProductController::class, 'createImageEdtior']);
    Route::post('/deleteimage',[ProductController::class, 'deleteImage']);
    Route::post('/deletedocument',[ProductController::class, 'deleteDocument']);
    Route::get('/user/{id}',[ProductController::class, 'getProductOfUser']);
    Route::post('/rating/{product}', [ProductController::class, 'rating']);
});
Route::get('/download', [ProductController::class, 'downloadDocument']);
Route::post('products_approve/{id}',[ProductController::class,'approveTeacher'])->middleware('auth:sanctum', 'permission:approve_teacher');
Route::put('chairman_approved/{product}',[ProductController::class,'chairmanApproved'])->middleware('auth:sanctum', 'permission:chairman_approved');
Route::get('products/avg-star/{product}', [ProductController::class, 'avgStar']);
Route::get('products/count-star/{product}', [ProductController::class, 'countStarInProduct']);
Route::get("/product-of-user/{id}", [ProductController::class, 'productOfUser']);
