<?php

use App\Http\Controllers\Api\Filter\FilterController;
use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => 'filter'
], function () {
    Route::post('/products', [FilterController::class, 'searchProduct']);
    Route::post('/', [FilterController::class, 'filter']);
    Route::group([
        'middleware'=>['auth:sanctum']
    ], function () {
        Route::get('/products/{status}', [FilterController::class, 'filterStatusProduct']);
    });
});
