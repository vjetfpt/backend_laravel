<?php

use App\Http\Controllers\Api\Comment\CommentController;
use Illuminate\Support\Facades\Route;

Route::prefix('comments')->middleware('auth:sanctum')->group(function() {
    Route::post('/', [CommentController::class, 'comment']);
    Route::get('/', [CommentController::class, 'getComments']);
    Route::post('/reply/{comment}', [CommentController::class, 'reply']);
    Route::put('/edit/{comment}', [CommentController::class, 'editComment']);
    Route::put('/edit/reply/{comment}', [CommentController::class, 'editReply']);
    Route::delete('/delete/{comment}', [CommentController::class, 'deleteComment']);
    Route::delete('/delete/reply/{comment}', [CommentController::class, 'deleteReply']);
});
Route::get('/comments-of-product/{product}', [CommentController::class, 'getCommentsOfProduct']);
Route::get('/new-comments', [CommentController::class, 'getNewComment']);
