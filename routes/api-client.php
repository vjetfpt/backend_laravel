<?php

use App\Http\Controllers\Api\Client\ClientController;
use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => 'client'
],function(){
    Route::get('/products-major/{id}', [ClientController::class,'productMajor']);
    Route::get('/products', [ClientController::class,'productRateHight']);
    Route::post('/filter', [ClientController::class,'filter']);
    Route::post('/search', [ClientController::class,'search']);
    Route::get('/sort', [ClientController::class,'sort']);
});